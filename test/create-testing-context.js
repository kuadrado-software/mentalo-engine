const { JSDOM } = require("jsdom");

module.exports = function () {
    const dom = new JSDOM("<!DOCTYPE html><html><body></body></html>", {
        resources: 'usable'
    });
    const { window } = dom;
    const { document } = window;
    global.window = window;
    global.document = document;
    global.Image = window.Image;
};