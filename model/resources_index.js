"use strict";

class MtlResourcesIndex {
    constructor() {
        this.images = [];
        this.sounds = [];
    }

    load_data(data) {
        this.images = data.images;
        this.sounds = data.sounds;
    }

    get_sound(name) {
        return this.sounds.find(snd => snd.name === name);
    }

    get_image(name) {
        return this.images.find(img => img.name === name);
    }

    update_image(name, updated_data) {
        const image = this.get_image(name);

        Object.entries(updated_data).forEach(kv => {
            const [k, v] = kv;
            image[k] = v;
        });
    }

    update_sound(name, updated_data) {
        const snd = this.get_sound(name);

        Object.entries(updated_data).forEach(kv => {
            const [k, v] = kv;
            snd[k] = v;
        });
    }
}

module.exports = MtlResourcesIndex