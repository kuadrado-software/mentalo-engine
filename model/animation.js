"use strict";
const Loadable = require("./loadable");

/**
 * The data type used for a MtlScene animation
 */
class MtlAnimation extends Loadable {
    /**
     * Initialize an empty instance of Image HtmlElement,
     * and registers the onload callback to update the instance with the loaded image actual dimensions.
     */
    constructor(shared_state) {
        super(new Image(), "image", "load");

        this.shared_state = shared_state;

        this.image.onload = () => this.update_dimensions();

        this.name = "";

        this.dimensions = {
            width: 0,
            height: 0,
        };

        this.frame_nb = 1;
        this.frame = 0;
        this.speed = 1;
        this.play_once = false;
        this.initialized = false;
        this.finished = false;
    }

    /**
     * Copies the dimensions of the loaded image at the root level of the instance.
     * Deletes the canvas dimensions precalculations created by the SceneAnimation component
     */
    update_dimensions() {
        this.dimensions = {
            width: this.image.width / this.frame_nb,
            height: this.image.height,
        };

        if (this.canvas_precalc) {
            delete this.canvas_precalc;
        }
    }

    /**
     * Populates the instance with loaded litteral data.
     * @param {Object} data 
     */
    init(data) {
        const resource = this.shared_state.resources_index.get_image(data.name);
        this.empty = !resource;
        this.name = data.name;
        this.image.src = resource ? resource.src : "";
        this.frame_nb = resource ? resource.frame_nb : 1;
        this.frame = 0;
        this.speed = data.speed;
        this.play_once = data.play_once;
        this.initialized = true;
    }

    /**
     * Increments the framcount argument if the animation next frame is ready to be rendered.
     * @param {Integer} framecount 
     */
    update_frame(framecount) {
        this.finished = this.play_once && this.frame === this.frame_nb - 1;

        if (this.frame_nb > 1 && framecount % this.speed === 0 && !this.finished) {
            this.frame = this.frame + 1 <= this.frame_nb - 1 ? this.frame + 1 : 0;
        }
    }

    /**
     * Reset the frame state.
     */
    reset_frame() {
        this.finished = false;
        this.frame = 0;
    }
}

module.exports = MtlAnimation;