// OBSOLETE
// TODO reimplement the tests using game.resources_index

// const createTestingContext = require("../../test/create-testing-context");
// const MtlAnimation = require("../animation");

// // This is a 4 pixels square image
// const test_base64_src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAIAAAD91JpzAAABhWlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9TpVIrDlYQccjQumhBVMRRq1CECqFWaNXB5NIPoUlD0uLiKLgWHPxYrDq4OOvq4CoIgh8gbm5Oii5S4v+SQosYD4778e7e4+4dINRLTLM6xgBNr5ipRFzMZFfEwCu60Y8gohiRmWXMSlISnuPrHj6+3sV4lve5P0ePmrMY4BOJZ5hhVojXiac2KwbnfeIwK8oq8TnxqEkXJH7kuuLyG+eCwwLPDJvp1BxxmFgstLHSxqxoasSTxBFV0ylfyLisct7irJWqrHlP/sJQTl9e4jrNISSwgEVIEKGgig2UUEGMVp0UCynaj3v4Bx2/RC6FXBtg5JhHGRpkxw/+B7+7tfIT425SKA50vtj2RxQI7AKNmm1/H9t24wTwPwNXestfrgPTn6TXWlrkCOjdBi6uW5qyB1zuAANPhmzKjuSnKeTzwPsZfVMW6LsFgqtub819nD4AaeoqeQMcHALDBcpe83h3V3tv/55p9vcDnYByuBkgSSgAAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQflCxUKJjKc1OFfAAAAGXRFWHRDb21tZW50AENyZWF0ZWQgd2l0aCBHSU1QV4EOFwAAABZJREFUCNdj/M/AwHCNgYHhPxsbw38AGrkD4VgwFK4AAAAASUVORK5CYII=";

// createTestingContext();

// OBSOLETE
// TODO reimplement the tests using game.resources_index

it("should be reimplemented", () => expect(1).toEqual(1))

// test("empty animation instance", () => {
//     const anim = new MtlAnimation();
//     expect(anim.loaded).toBe(false);
//     expect(anim.name).toBe("");
//     expect(anim.dimensions).toEqual({ width: 0, height: 0 });
//     expect(anim.frame_nb).toBe(1);
//     expect(anim.frame).toBe(0);
//     expect(anim.speed).toBe(1);
//     expect(anim.play_once).toBe(false);
//     expect(anim.initialized).toBe(false);
//     expect(anim.finished).toBe(false);
// });

// test("image update_dimensions onload", done => {
//     const anim = new MtlAnimation();
//     const spy_update_dimensions = jest.spyOn(anim, "update_dimensions");

//     // overload the onload callback in order to catch the call
//     const save_cb = anim.image.onload;
//     anim.image.onload = () => {
//         save_cb();
//         expect(anim.dimensions.height).toBe(2);
//         expect(spy_update_dimensions).toHaveBeenCalledTimes(1);
//         done();
//     };

//     anim.init({
//         src: test_base64_src,
//         name: "test animation",
//         frame_nb: 5,
//         play_once: true,
//         speed: 12,
//     });

//     expect(anim.image.src).toBe(test_base64_src);
//     expect(anim.name).toBe("test animation");
//     expect(anim.play_once).toBe(true);
//     expect(anim.frame_nb).toBe(5);
//     expect(anim.speed).toBe(12);
// });

// test("test update frame", () => {
//     const anim = new MtlAnimation();
//     anim.init({
//         src: test_base64_src,
//         name: "test animation",
//         frame_nb: 5,
//         play_once: true,
//         speed: 4, // frame will change every 4 calls
//     });

//     let framecount = 4;
//     while (framecount <= 20) {
//         expect(anim.finished).toBe(false);
//         anim.update_frame(++framecount);
//     }

//     expect(anim.finished).toBe(true);
//     expect(anim.frame).toBe(4);

//     anim.play_once = false;
//     anim.frame = 0;
//     anim.finished = false;
//     anim.speed = 1;
//     framecount = 0;
//     let prev_frame = 0;
//     while (framecount <= 50) {
//         prev_frame = anim.frame;
//         anim.update_frame(++framecount);
//         expect(anim.frame).toBe(prev_frame + 1 <= anim.frame_nb - 1 ? prev_frame + 1 : 0);
//         expect(anim.finished).toBe(false);
//     }
// });

// test("test update frame", () => {
//     const anim = new MtlAnimation();
//     anim.init({
//         src: test_base64_src,
//         name: "test animation",
//         frame_nb: 5,
//         play_once: true,
//         speed: 4, // frame will change every 4 calls
//     });
//     let framecount = 0;
//     while (framecount <= 20) {
//         anim.update_frame(++framecount);
//     }
//     expect(anim.finished).toBe(true);

//     anim.reset_frame();
//     expect(anim.finished).toBe(false);
//     expect(anim.frame).toBe(0);
// });