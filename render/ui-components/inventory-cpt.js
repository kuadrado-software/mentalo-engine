"use strict";

const MtlUiComponent = require("./ui-component");

/**
 * The component that draws the inventory panel
 */
class InventoryCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("InventoryCpt")
    }

    /**
     * Draws the panel on the canvas, T
     * the grid slots and the game object images are draws as children components (InventorySlotCpt and InventoryObjectCpt)
     */
    draw() {
        super.draw();
        if (this.params.is_visible()) {
            this.clear_bounding_zone();
            this.draw_children();
        } else {
            this.clear_bounding_zone({ clear_color: this.params.invisible_clear_color })
        }
    }
}

module.exports = InventoryCpt;