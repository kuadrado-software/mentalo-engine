"use strict";

const MtlUiComponent = require("./ui-component");

/**
 * The rendering component for the game choices panel
 */
class ChoicesPanelCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("ChoicesPanelCpt")
    }

    /**
     * Draw the choices panel and the choices as children components on the registered 2D drawing context
     */
    draw() {
        super.draw();
        if (this.params.is_visible()) {
            this.clear_bounding_zone({ clear_color: this.params.invisible_clear_color });
            this.clear_bounding_zone({ use_bounding_zone: this.params.minimum_bounding_zone() })
            this.draw_children();
        } else {
            this.clear_bounding_zone({ clear_color: this.params.invisible_clear_color })
        }
    }
}

module.exports = ChoicesPanelCpt;