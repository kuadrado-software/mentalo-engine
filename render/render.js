"use strict";

const FrameRateController = require("../lib/frame-rate-controller");
const { get_optimal_visible_foreground_color } = require("../lib/color-tools");
const { get_canvas_char_size } = require("../lib/font-tools");
const SCENE_TYPES = require("../model/scene-types");
const ChoiceCpt = require("./ui-components/choice-cpt");
const ChoicesPanelCpt = require("./ui-components/choices-panel-cpt");
const ClosingIconCpt = require("./ui-components/closing-icon-cpt");
const GameObjectCpt = require("./ui-components/game-object-cpt");
const InventoryCpt = require("./ui-components/inventory-cpt");
const InventoryObjectCpt = require("./ui-components/inventory-object-cpt");
const InventorySlotCpt = require("./ui-components/inventory-slot-cpt");
const SceneAnimationCpt = require("./ui-components/scene-animation-cpt");
const TextBoxCpt = require("./ui-components/text-box-cpt");
const UserErrorPopup = require("./ui-components/user-error-popup");
const CanvasDimensions = require("./canvas-dimensions");
const TextFeedbackCpt = require("./ui-components/text-feedback-cpt");

/**
 * The class that handles all interactions with the canvas 2D drawing context to draw the game.
 */
class MtlRender {
    constructor(params) {
        this.params = params;
        const frame_rate = this.params.frame_rate || 30;
        this.fps_controller = new FrameRateController(frame_rate);

        this.params.container.style.backgroundColor = this.params.get_game_settings("general").background_color;
        this.canvas = document.createElement("canvas");
        this.canvas.style.backgroundColor = "black";

        window.mentalo_drawing_context = this.canvas.getContext("2d");

        this.canvas_dimensions = {};
        this.canvas_zones = {};
        this.event_listeners = {
            game_objects: [],
            inventory: [],
            text_box: [],
        };

        this.state = {
            user_error_popup: { is_set: false, text: "", on_close: function () { } },
            text_feedback: ""
        };

        this.loading_frame = 0;
    }

    /**
     * Initializes the rendering of the game, creates the components, the canvas zones and requests full screen.
     */
    init() {
        if (this.params.fullscreen) {
            this.set_full_screen();
        }

        this.set_canvas_dimensions();
        this.create_canvas_zones();

        this.params.container.appendChild(this.canvas);

        const ctx = window.mentalo_drawing_context;
        // Obsolete properties for browsers compatibility
        ctx.mozImageSmoothingEnabled = false;
        ctx.webkitImageSmoothingEnabled = false;
        ctx.msImageSmoothingEnabled = false;
        ctx.imageSmoothingEnabled = false;

        this.create_components();
    }

    /**
     * Removes the event listeners attached to the components.
     */
    clear_event_listeners() {
        Object.values(this.components).forEach(cpt => {
            cpt.clear_event_listeners();
        });
    }

    /**
     * Removes the elements rendered as children of the rendering components 
     * (Example a TextBoxCpt is a child the scene_animation component)
     * @param {Object} options can defined a set of constructor names to exclude from clean up
     */
    clear_children(options = { exclude: [] }) {
        Object.values(this.components).forEach(cpt => {
            cpt.clear_children(options);
        });
    }

    /**
     * Reset the scene text box visibility state to true.
     * The text box visibility state is controlled by the scene_animation component and not directly by the text box component.
     */
    reset_text_box_visibility() {
        this.components.scene_animation.set_text_box_visibility(true)
    }

    /**
     * Tries to enable the window fullscreen mode.
     */
    set_full_screen() {
        const body = document.body;
        body.requestFullScreen = body.requestFullScreen
            || body.webkitRequestFullScreen
            || body.msRequestFullscreen
            || body.mozRequestFullScreen;
        try {
            body.requestFullScreen();
        } catch (err) {
            console.error(err.message)
        }
    }

    /**
     * Exits the window fullscreen mode
     */
    exit_fullscreen() {
        if (document.fullscreenElement) {
            document.exitFullscreen();
        }
    }

    /**
     * Updates the error popup state with new values given as argument.
     * @param {Object} params Must have a text<String> entry and a on_close<Function> entry.
     */
    set_user_error_popup(params) {
        this.state.user_error_popup = {
            is_set: true,
            text: params.text,
            stream_text: !!params.stream_text,
            on_close: function () {
                params.on_close && params.on_close();
            },
        };
        this.clear_event_listeners();
        // Clears children excluding everything but errorpopup
        this.clear_children({ exclude: ["TextBoxCpt", "InventoryCpt", "ChoicesPanelCpt", "GameObjectCpt"] });
    }

    /**
     * Unsets error popup
     */
    clear_user_error_popup() {
        this.reset_user_error_popup();
        this.clear_event_listeners();
        this.clear_children({ exclude: ["TextBoxCpt", "InventoryCpt", "ChoicesPanelCpt", "GameObjectCpt"] });
    }

    /**
     * Sets error popup to empty values
     */
    reset_user_error_popup() {
        this.state.user_error_popup = {
            is_set: false,
            text: "",
            on_close: function () { }
        };
    }

    /**
     * Callback called when error popup is closed
     */
    on_close_user_error_popup() {
        this.state.user_error_popup.on_close();
        this.clear_user_error_popup();
    }

    /**
     * Parses game data and calculates optimal dimensions for the canvas.
     */
    set_canvas_dimensions() {
        const { get_game_settings, get_game_scenes } = this.params;

        this.canvas_dimensions = new CanvasDimensions({
            get_game_settings,
            get_game_scenes,
        });

        this.canvas.width = this.canvas_dimensions.width;
        this.canvas.height = this.canvas_dimensions.height;
    }

    /**
     * @returns {Object} The choices_panel settings scaled to display canvas size
     */
    get_scaled_choices_panel_settings() {
        return this.canvas_dimensions.choices_panel_metrics.settings;
    }

    /**
     * @returns {Object} The inventory settings scaled to display canvas size
     */
    get_scaled_inventory_settings() {
        const { scale_factor } = this.canvas_dimensions;
        const { get_game_settings } = this.params;

        const base_settings = get_game_settings("inventory");

        return Object.assign({ ...base_settings }, {
            slot_rounded_corner_radius: base_settings.slot_rounded_corner_radius * scale_factor,
            slot_border_width: base_settings.slot_border_width * scale_factor,
            gap: base_settings.gap * scale_factor,
            padding: base_settings.padding * scale_factor,
        });
    }

    /**
     * @returns {Object} The text boxes settings scaled to display canvas size
     */
    get_scaled_text_boxes_settings() {
        const { scale_factor } = this.canvas_dimensions;
        const { get_game_settings } = this.params;
        const text_box_settings = get_game_settings("text_boxes");

        return Object.assign({ ...text_box_settings }, {
            font_size: text_box_settings.font_size * scale_factor,
            padding: text_box_settings.padding * scale_factor,
            margin: text_box_settings.margin * scale_factor,
            rounded_corners_radius: text_box_settings.rounded_corners_radius * scale_factor,
            border_width: text_box_settings.border_width * scale_factor,
        })
    }

    /**
     * Precalculates the bounding boxes for each canvas zone.
     * set_canvas_dimensions() must have been called before this.
     */
    create_canvas_zones() {
        const metrics = this.canvas_dimensions;
        const inventory_settings = this.get_scaled_inventory_settings();
        const choices_panel_settings = this.get_scaled_choices_panel_settings();

        this.canvas_zones = {
            root: {
                left: 0,
                top: 0,
                right: metrics.width,
                bottom: metrics.height,
                width: metrics.width,
                height: metrics.height,
                padding: 0,
            },
            scene_animation: {
                left: 0,
                top: 0,
                right: metrics.image.width,
                bottom: metrics.image.height,
                width: metrics.image.width,
                height: metrics.image.height,
                clear_color: "black",
                padding: 0,
            },
            inventory: {
                left: metrics.image.width,
                top: 0,
                right: metrics.width,
                bottom: metrics.inventory.height,
                width: metrics.width - metrics.image.width,
                height: metrics.inventory.height,
                clear_color: inventory_settings.background_color,
                padding: inventory_settings.padding,
            },
            choices_panel: {
                left: 0,
                top: metrics.image.height,
                right: metrics.width,
                bottom: metrics.height,
                width: metrics.width,
                height: metrics.height - metrics.image.height,
                clear_color: choices_panel_settings.background_color,
                padding: choices_panel_settings.container_padding
            },
        };
    }

    /**
     * @returns {Boolean} true if scene is of type Playable
     */
    scene_is_not_cinematic() {
        return this.params.get_scene()._type === SCENE_TYPES.PLAYABLE;
    }

    /**
     * Creates the tree of all the components to render. All component extends the UiComponent class.
     * The root component instances (Scene animation, Inventory, Choices panel) will only be created once,
     * but children components are functions of their parents so they are recreated when get_children is called on a parent component.
     * For example A TextBoxCpt is a children a SceneAnimationCpt, so the text box component will be recreated each time 
     * scene_animation.get_children() is called. This allow to display those components as functions of dynamical states.
     */
    create_components() {
        const { get_game_settings, get_inventory, get_scene, get_game_scenes, get_scene_index } = this.params;
        const { scale_factor } = this.canvas_dimensions;
        const scene_is_not_cinematic = this.scene_is_not_cinematic.bind(this);

        this.components = {
            scene_animation: new SceneAnimationCpt({
                bounding_zone: this.canvas_zones.scene_animation,
                get_animation: () => get_scene().animation,
                next_frame_ready: () => this.fps_controller.nextFrameReady(),
                get_children: () => {
                    const scene = get_scene();
                    return scene.game_objects.map(o => {
                        const parent_zone = this.canvas_zones.scene_animation;
                        const obj_pos = {
                            x: (o.position.x * scale_factor) + parent_zone.left,
                            y: (o.position.y * scale_factor) + parent_zone.top,
                        };

                        const obj_dim = {
                            w: o.image.width * scale_factor,
                            h: o.image.height * scale_factor,
                        };

                        const obj_bounds = {
                            left: obj_pos.x,
                            right: obj_pos.x + obj_dim.w,
                            top: obj_pos.y,
                            bottom: obj_pos.y + obj_dim.h,
                            width: obj_dim.w,
                            height: obj_dim.h,
                        };

                        const game_objects_cpt_params = {
                            z_index: 2,
                            bounding_zone: obj_bounds,
                            position: obj_pos,
                            dimensions: obj_dim,
                            image: o.image,
                            name: o.name,
                            is_in_inventory: () => get_inventory().has(o),
                        };

                        const obj_cpt = new GameObjectCpt(game_objects_cpt_params);

                        obj_cpt.add_event_listener({
                            event_type: "mousemove",
                            listener: e => {
                                if (!get_inventory().has(o)) {
                                    const cursor_is_over_obj = e.offsetX >= obj_bounds.left
                                        && e.offsetX <= obj_bounds.right
                                        && e.offsetY >= obj_bounds.top
                                        && e.offsetY <= obj_bounds.bottom;
                                    obj_cpt.state.draw_border = cursor_is_over_obj;
                                }
                            }
                        });

                        obj_cpt.add_event_listener({
                            event_type: "click",
                            listener: e => {
                                if (!get_inventory().has(o)
                                    && e.offsetX >= obj_bounds.left
                                    && e.offsetX <= obj_bounds.right
                                    && e.offsetY >= obj_bounds.top
                                    && e.offsetY <= obj_bounds.bottom) {
                                    this.params.on_game_object_click(o);
                                    this.clear_event_listeners();
                                    this.clear_children({ exclude: "TextBoxCpt" });
                                }
                            }
                        });

                        return obj_cpt;
                    })
                        .concat(scene.text_box ? [
                            (() => {
                                const text_box_settings = this.get_scaled_text_boxes_settings();

                                // calculation of the minimum bound for textbox, without content
                                const text_box_bounds = (() => {
                                    const parent_zone = this.canvas_zones.scene_animation;
                                    const padding = text_box_settings.padding;
                                    const margin = text_box_settings.margin;
                                    const left = parent_zone.left + margin;
                                    const right = parent_zone.right - margin;
                                    const bottom = parent_zone.bottom - margin;
                                    const top = bottom - (2 * padding);
                                    return {
                                        left,
                                        right,
                                        top,
                                        bottom,
                                        width: right - left,
                                        height: bottom - top,
                                    }
                                })();

                                const closing_icon_radius = 10 * scale_factor;
                                const closing_icon_center = {
                                    x: text_box_bounds.right - (closing_icon_radius / 2),
                                    y: text_box_bounds.top + (closing_icon_radius / 2),
                                };

                                const closing_icon_params = {
                                    color: text_box_settings.font_color,
                                    radius: closing_icon_radius,
                                    center: closing_icon_center,
                                    background_color: text_box_settings.background_color,
                                    line_width: Math.floor(2 * scale_factor),
                                    bounding_zone: {
                                        left: closing_icon_center.x - closing_icon_radius,
                                        right: closing_icon_center.x + closing_icon_radius,
                                        top: closing_icon_center.y - closing_icon_radius,
                                        bottom: closing_icon_center.y + closing_icon_radius,
                                        width: 2 * closing_icon_radius,
                                        height: 2 * closing_icon_radius,
                                    },
                                };

                                const text_box = new TextBoxCpt({
                                    z_index: 3,
                                    text: get_scene().text_box,
                                    settings: text_box_settings,
                                    bounding_zone: text_box_bounds,
                                    get_visibility_state: () => this.components.scene_animation.state.text_box_visible,
                                    get_children: () => [new ClosingIconCpt(closing_icon_params)],
                                });

                                const close_text_box_icon = text_box.children[0];
                                text_box.children[0].add_event_listener({
                                    event_type: "click",
                                    listener: e => {
                                        const bounds = close_text_box_icon.params.bounding_zone;
                                        const click_over_icon = e.offsetX >= bounds.left
                                            && e.offsetX <= bounds.right
                                            && e.offsetY >= bounds.top
                                            && e.offsetY <= bounds.bottom;

                                        const { scene_animation } = this.components;

                                        if (click_over_icon && scene_animation.state.text_box_visible) {
                                            scene_animation.set_text_box_visibility(false);
                                        }
                                    }
                                });
                                return text_box;
                            })(),
                        ] : [])
                        .concat(this.state.user_error_popup.is_set ? [
                            (() => {
                                const { text, stream_text } = this.state.user_error_popup;
                                const use_settings = this.get_scaled_text_boxes_settings();
                                const parent_bounds = this.canvas_zones.scene_animation
                                const char_size = get_canvas_char_size(window.mentalo_drawing_context, use_settings);

                                const popup_width = parent_bounds.width / 2;
                                const padding = char_size.width * 2;
                                const max_chars_per_row = (popup_width - (2 * padding)) / char_size.width;
                                const text_lines = [""];

                                let line_i = 0;
                                text.split(" ").forEach(word => {
                                    if (`${text_lines[line_i]}${word} `.length > max_chars_per_row) {
                                        line_i++;
                                        text_lines.push("");
                                    }
                                    text_lines[line_i] += `${word} `;
                                });

                                const line_h = char_size.text_line_height;
                                const popup_height = (text_lines.length * line_h) + (2 * padding) - char_size.interline_height;

                                const background_color = use_settings.background_color;
                                const text_color = use_settings.font_color;

                                const popup_bounds = {
                                    left: parent_bounds.left + (parent_bounds.width / 2) - (popup_width / 2),
                                    top: parent_bounds.top + (parent_bounds.height / 2) - (popup_height / 2),
                                    right: parent_bounds.left + (parent_bounds.width / 2) + (popup_width / 2),
                                    bottom: parent_bounds.top + (parent_bounds.height / 2) - (popup_height / 2) + popup_height,
                                    width: popup_width,
                                    height: popup_height,
                                    clear_color: background_color,
                                };

                                const popup_params = {
                                    z_index: 4,
                                    settings: use_settings,
                                    bounding_zone: popup_bounds,
                                    modal_bounds: this.canvas_zones.scene_animation,
                                    clear_modal_color: "#0004",
                                    text_lines,
                                    padding,
                                    stream_text,
                                    get_children: () => {
                                        const center = {
                                            x: popup_bounds.right - 5,
                                            y: popup_bounds.top + 5,
                                        };

                                        const radius = 15;

                                        const closing_icon_bounds = {
                                            left: center.x - radius,
                                            right: center.x + radius,
                                            top: center.y - radius,
                                            bottom: center.y + radius,
                                            width: radius * 2,
                                            height: radius * 2,
                                        };

                                        const closing_icon_params = {
                                            color: text_color,
                                            radius,
                                            center,
                                            background_color,
                                            line_width: 2,
                                            bounding_zone: closing_icon_bounds,
                                        };

                                        const closing_icon = new ClosingIconCpt(closing_icon_params);

                                        closing_icon.add_event_listener({
                                            event_type: "click",
                                            listener: e => {
                                                if (e.offsetX >= closing_icon_bounds.left
                                                    && e.offsetX <= closing_icon_bounds.right
                                                    && e.offsetY >= closing_icon_bounds.top
                                                    && e.offsetY <= closing_icon_bounds.bottom) {
                                                    this.on_close_user_error_popup();
                                                }
                                            }
                                        });

                                        return [closing_icon];
                                    }
                                };
                                const popup = new UserErrorPopup(popup_params);
                                return popup;
                            })()
                        ] : [])
                        .concat([new TextFeedbackCpt({
                            z_index: 4,
                            get_text_feedback: () => {
                                const obj_hover = this.components.scene_animation.children
                                    .filter(c => c.str_id === "GameObjectCpt" && c.state.draw_border)
                                    .map(c => c.params.name)
                                    .concat(this.components.inventory.children
                                        .filter(c => c.str_id === "InventorySlotCpt" && c.children[0].state.hover)
                                        .map(c => {
                                            const slot_object = c.children[0];
                                            const o = slot_object.params.get_game_object(slot_object.params.slot_index);
                                            return o.ref.name;
                                        })
                                    );
                                return obj_hover.length > 0 ? obj_hover[0] : ""
                            },
                        })]);
                },
            }),
            inventory: new InventoryCpt({
                bounding_zone: this.canvas_zones.inventory,
                get_inventory,
                is_visible: scene_is_not_cinematic,
                invisible_clear_color: get_game_settings("general").background_color,
                get_children: () => {
                    const settings = this.get_scaled_inventory_settings();

                    const slots_zone = {
                        left: this.canvas_zones.inventory.left + settings.padding,
                        top: this.canvas_zones.inventory.top + settings.padding,
                        right: this.canvas_zones.inventory.right - settings.padding,
                        bottom: this.canvas_zones.inventory.bottom - settings.padding,
                        width: this.canvas_zones.inventory.width - (2 * settings.padding),
                        height: this.canvas_zones.inventory.height - (2 * settings.padding)
                    };

                    const gap = settings.gap;
                    const h = slots_zone.height;
                    const gap_h = (settings.rows - 1) * gap;
                    const slot_side = (h - gap_h) / settings.rows;
                    const slots_total_w = (slot_side * settings.columns) + (gap * (settings.columns - 1));
                    const center_slot_x_offset = (slots_zone.width - (slots_total_w)) / 2;

                    const slots = [];
                    let slot_i = 0;
                    Array.from({ length: settings.rows }).forEach((_row, i) => {
                        const top = slots_zone.top + (i * (slot_side + gap));
                        Array.from({ length: settings.columns }).forEach((_col, j) => {
                            const left = slots_zone.left + (j * (slot_side + gap)) + center_slot_x_offset;

                            const slot_bounds = {
                                left,
                                top,
                                right: left + slot_side,
                                bottom: top + slot_side,
                                width: slot_side,
                                height: slot_side
                            };

                            const stroke_color = get_optimal_visible_foreground_color(settings.background_color);

                            const slot_params = {
                                bounding_zone: slot_bounds,
                                stroke_color,
                                is_visible: scene_is_not_cinematic,
                                settings,
                                get_children: () => {
                                    const inventory_object = new InventoryObjectCpt({
                                        slot_index: slot_i,
                                        is_visible: scene_is_not_cinematic,
                                        settings,
                                        stroke_color,
                                        get_game_object: index => {
                                            const obj = Array.from(get_inventory())[index];
                                            if (obj) {
                                                const obj_dim = obj.get_dimensions();
                                                const obj_ratio = obj_dim.width / obj_dim.height;
                                                const scaled_dim = { width: 0, height: 0 };
                                                const pos = { x: 0, y: 0 };
                                                if (obj_ratio > 1) {
                                                    // object image landscape oriented
                                                    scaled_dim.width = slot_side;
                                                    scaled_dim.height = slot_side * (obj_dim.height / obj_dim.width);
                                                    pos.x = slot_bounds.left;
                                                    pos.y = slot_bounds.top + ((slot_side / 2) - (scaled_dim.height / 2));
                                                } else if (obj_ratio < 1) {
                                                    // portrait oriented
                                                    scaled_dim.height = slot_side;
                                                    scaled_dim.width = slot_side * obj_ratio;
                                                    pos.y = slot_bounds.top;
                                                    pos.x = slot_bounds.left + ((slot_side / 2) - (scaled_dim.width / 2));
                                                } else {
                                                    // square
                                                    scaled_dim.width = slot_side;
                                                    scaled_dim.height = slot_side;
                                                    pos.x = slot_bounds.left;
                                                    pos.y = slot_bounds.top;
                                                }
                                                return {
                                                    ref: obj,
                                                    position: pos,
                                                    dimensions: scaled_dim,
                                                };
                                            } else {
                                                return undefined;
                                            }
                                        },
                                        bounding_zone: slot_bounds,
                                        get_children: () => [
                                            new ClosingIconCpt({
                                                is_visible: scene_is_not_cinematic,
                                                color: "#bf5e43", // Some kind of red
                                                center: {
                                                    x: slot_bounds.left + (slot_side / 2),
                                                    y: slot_bounds.top + (slot_side / 2)
                                                },
                                                radius: slot_side / 4,
                                                line_width: slot_side / 20 > 2 ? slot_side / 20 : 2,
                                                bounding_zone: { ...slot_bounds, clear_color: "rgba(0,0,0,0.2)" },
                                            }),
                                        ]
                                    });

                                    inventory_object.add_event_listener({
                                        event_type: "mousemove",
                                        listener: e => {
                                            const obj = inventory_object.params.get_game_object(inventory_object.params.slot_index);
                                            const bounds = inventory_object.params.bounding_zone;

                                            const visible = inventory_object.params.is_visible();
                                            const can_be_dropped = !!obj && get_scene().game_objects.includes(obj.ref);
                                            const hover = visible && !!obj && (
                                                e.offsetX >= bounds.left
                                                && e.offsetX <= bounds.right
                                                && e.offsetY >= bounds.top
                                                && e.offsetY <= bounds.bottom
                                            );

                                            inventory_object.state.hover = hover;
                                            inventory_object.state.draw_inventory_close_icon = can_be_dropped && hover;
                                        }
                                    });

                                    inventory_object.children[0].add_event_listener({
                                        event_type: "click",
                                        listener: e => {
                                            if (!inventory_object.params.is_visible()) return false;

                                            const obj = inventory_object.params.get_game_object(inventory_object.params.slot_index);
                                            const bounds = inventory_object.params.bounding_zone;
                                            const mouse_over_slot = e.offsetX >= bounds.left
                                                && e.offsetX <= bounds.right
                                                && e.offsetY >= bounds.top
                                                && e.offsetY <= bounds.bottom;

                                            if (!!obj && mouse_over_slot && get_scene().game_objects.includes(obj.ref)) {
                                                this.params.on_drop_inventory_object(obj.ref);
                                                this.clear_event_listeners();
                                                this.clear_children({ exclude: "TextBoxCpt" });
                                            }
                                        }
                                    });

                                    return [inventory_object];
                                },
                            };

                            const slot = new InventorySlotCpt(slot_params);
                            slots.push(slot);
                            slot_i++;
                        });
                    });
                    return slots;
                },
            }),
            choices_panel: new ChoicesPanelCpt({
                bounding_zone: this.canvas_zones.choices_panel,
                minimum_bounding_zone: () => {
                    const initial_zone = this.canvas_zones.choices_panel;
                    const minium_height = this.canvas_dimensions.choices_panel_metrics
                        .get_minimum_panel_height_for_scene(
                            get_scene_index()
                        );
                    const maximum_height = this.canvas_dimensions.choices_panel_metrics
                        .get_total_height();
                    const dif = maximum_height - minium_height;

                    return Object.assign({ ...initial_zone }, {
                        bottom: initial_zone.bottom - dif,
                        height: initial_zone.height - dif,
                    });
                },
                is_visible: scene_is_not_cinematic,
                invisible_clear_color: get_game_settings("general").background_color,
                get_children: () => Array.from({
                    length: this.canvas_dimensions.choices_panel_metrics
                        .get_max_choices_row_per_scene(get_scene_index()) * 2
                }
                ).map((_, i) => {
                    const choices_settings = this.get_scaled_choices_panel_settings();
                    const writable_parent_zone = { // The writable zone of the choices panel. = The choices_panel bounding box excluding the padding.
                        left: this.canvas_zones.choices_panel.left + choices_settings.container_padding,
                        right: this.canvas_zones.choices_panel.right - choices_settings.container_padding,
                        top: this.canvas_zones.choices_panel.top + choices_settings.container_padding,
                        bottom: this.canvas_zones.choices_panel.bottom - choices_settings.container_padding,
                        width: 0,
                        height: 0,
                    };

                    writable_parent_zone.width = writable_parent_zone.right - writable_parent_zone.left;
                    writable_parent_zone.height = writable_parent_zone.bottom - writable_parent_zone.top;

                    const choice_width = writable_parent_zone.width / 2;

                    const { choices_panel_metrics } = this.canvas_dimensions;
                    const choices_height_per_row = choices_panel_metrics.rows.map(row => row.text_height + row.padding_height);

                    const choice_bounds = {
                        left: writable_parent_zone.left + ((i % 2) * choice_width), // if i % 2 choice is on the right side of the panel
                        top: writable_parent_zone.top + ((i > 1 ? 1 : 0) * choices_height_per_row[0]), // If index is > 1, choice is on the lower row
                        right: writable_parent_zone.left + ((i % 2) * choice_width) + choice_width,
                        bottom: writable_parent_zone.top + ((i > 1 ? 1 : 0) * choices_height_per_row[1]) + choices_height_per_row[i > 1 ? 1 : 0],
                        width: choice_width,
                        height: choices_height_per_row[i > 1 ? 1 : 0],
                    };

                    const choice_cpt = new ChoiceCpt({
                        is_visible: scene_is_not_cinematic,
                        minimum_bounding_zone: () => {
                            if (!choice_cpt.minimum_bounding_zone) {
                                const row_index = i > 1 ? 1 : 0;
                                const metric = this.canvas_dimensions.choices_panel_metrics.get_one_choices_row_metrics(
                                    get_scene_index(), row_index,
                                );

                                const min_height = metric.text_height + metric.padding_height;

                                const dif_upper_row = (() => {
                                    const choices_cpt = this.components.choices_panel.children;
                                    const cpt_index = choices_cpt.indexOf(choice_cpt);
                                    return cpt_index >= 2
                                        ? choice_bounds.top - choices_cpt[0].params.minimum_bounding_zone().bottom
                                        : 0;
                                })();

                                choice_cpt.minimum_bounding_zone = Object.assign({ ...choice_bounds }, {
                                    height: min_height,
                                    top: choice_bounds.top - dif_upper_row,
                                    bottom: choice_bounds.top - dif_upper_row + min_height,
                                });
                            }
                            return choice_cpt.minimum_bounding_zone;
                        },
                        get_formatted_choice: () => {
                            const scene_choices = this.canvas_dimensions.choices_panel_metrics.scenes_formatted_choices[
                                get_game_scenes().indexOf(get_scene())
                            ];
                            return scene_choices.length - 1 >= i ? scene_choices[i] : undefined;
                        },
                        settings: choices_settings,
                    });

                    choice_cpt.add_event_listener({
                        event_type: "mousemove",
                        listener: e => {
                            choice_cpt.state.active =
                                choice_cpt.params.is_visible()
                                && !!choice_cpt.params.get_formatted_choice()
                                && choice_cpt.is_hover(e);
                        }
                    });

                    choice_cpt.add_event_listener({
                        event_type: "click",
                        listener: () => {
                            if (choice_cpt.state.active) {
                                const choice = choice_cpt.params.get_formatted_choice();
                                const success = this.params.on_choice_click(choice);
                                if (success) {
                                    this.clear_event_listeners();
                                    this.clear_children();
                                }
                            }
                        }
                    });

                    return choice_cpt;
                })
            }),
        };
    }

    /**
     * Draw a loading state on the black screen while some game resources are loading.
     * This shouldn't show up a lot except if some resources are really big...
     */
    draw_loading() {
        const ctx = window.mentalo_drawing_context;
        ctx.save();
        ctx.font = '25px monospace';
        ctx.fillStyle = "black";
        ctx.fillRect(0, 0, window.innerWidth, window.innerHeight);
        ctx.fillStyle = "white";
        ctx.fillText("Loading", 50, window.innerHeight / 2);
        const dots = Array.from({ length: ++this.loading_frame }).map(() => '.').join('');
        ctx.font = '8px monospace';
        ctx.fillText(dots, 50, window.innerHeight / 2 + 20);
        ctx.restore();
    }

    /**
     * Executes the draw method of each registered component.
     */
    draw_game() {
        Object.values(this.components).forEach(cpt => cpt.draw());
    }
}

module.exports = MtlRender;