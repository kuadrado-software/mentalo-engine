"use strict";

const { draw_text_in_bounds } = require("../../lib/text-tools");
const { draw_rect } = require("../../lib/shape-tools");
const MtlUiComponent = require("./ui-component");

/**
 * The rendering component for a scene choice.
 */
class ChoiceCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("ChoiceCpt")
    }

    /**
     * Draw the choice on the registered 2D drawing context
     */
    draw() {
        super.draw();
        const {
            get_formatted_choice,
            settings,
        } = this.params;

        const bounding_zone = this.params.minimum_bounding_zone();

        const writable_zone = {
            left: bounding_zone.left + settings.choice_padding,
            right: bounding_zone.right - settings.choice_padding,
            top: bounding_zone.top + settings.choice_padding,
            bottom: bounding_zone.bottom - settings.choice_padding,
            width: bounding_zone.width - (2 * settings.choice_padding),
            height: bounding_zone.height - (2 * settings.choice_padding),
        };

        const choice = get_formatted_choice();

        const ctx = window.mentalo_drawing_context;

        if (choice) {
            if (this.state.active) {
                const { left, top, width, height } = bounding_zone;

                draw_rect(ctx, left, top, width, height, {
                    fill_color: settings.active_choice_background_color,
                    rounded_corners_radius: settings.active_choice_rounded_corners_radius,
                    border: {
                        width: settings.active_choice_border_width,
                        color: settings.font_color,
                    }
                });
            }

            draw_text_in_bounds(ctx, choice.text_lines, writable_zone, settings, {}, false);
        }
    }

    /**
     * @param {Event} e 
     * @returns {Boolean} true if the mouse event is inside the choice bounding box
     */
    is_hover(e) {
        const bounds = this.params.minimum_bounding_zone();
        return e.offsetX >= bounds.left
            && e.offsetX <= bounds.right
            && e.offsetY >= bounds.top
            && e.offsetY <= bounds.bottom;
    }
}

module.exports = ChoiceCpt;