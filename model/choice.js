"use strict";
/**
 * The data type used for the choices of a MtlScene
 */
class MtlChoice {
    /**
     * Initializes the instance with given data or default empty data.
     * @param {Object} data 
     */
    constructor(data = {
        text: "",
        destination_scene_index: -1,
        use_objects: {
            value: false,
            items: [],
            missing_object_message: "",
        }
    }) {
        this.load_data(data);
    }

    /**
     * Populates the instance with litteral description data
     * @param {Object} data 
     */
    load_data(data) {
        this.text = data.text;
        this.destination_scene_index = data.destination_scene_index;
        this.use_objects = data.use_objects;
    }
}

module.exports = MtlChoice;