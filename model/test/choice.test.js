const MtlChoice = require("../choice");
const createTestingContext = require("../../test/create-testing-context");

createTestingContext();

afterEach(() => {
    jest.clearAllMocks();
});

test("default choice", () => {
    const spy_load_data = jest.spyOn(MtlChoice.prototype, "load_data");
    const choice = new MtlChoice();
    expect(choice.text).toBe("");
    expect(choice.destination_scene_index).toBe(-1);
    expect(choice.use_objects).toEqual({
        value: false,
        items: [],
        missing_object_message: "",
    });
    expect(spy_load_data).toHaveBeenCalledTimes(1);
});

test("choice load data", () => {
    const spy_load_data = jest.spyOn(MtlChoice.prototype, "load_data");
    const choice = new MtlChoice({
        text: "test choice",
        destination_scene_index: 2,
        use_objects: {
            value: true,
            items: ["test-object-name"],
            missing_object_message: "missing test object"
        }
    });
    expect(spy_load_data).toHaveBeenCalledTimes(1);
    expect(choice.text).toBe("test choice");
    expect(choice.destination_scene_index).toBe(2);
    expect(choice.use_objects).toEqual({
        value: true,
        items: ["test-object-name"],
        missing_object_message: "missing test object",
    });
});