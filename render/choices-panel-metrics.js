"use strict";

const { get_canvas_char_size } = require("../lib/font-tools");

/**
 * A class to facilitate the handling of the chocies panel outer and inner dimensions
 */
class ChoicesPanelMetrics {
    constructor(params) {
        this.params = params;
        const { scale_factor = 1 } = this.params;
        this.container_width = this.params.container_width * scale_factor;
        this.settings = this.get_scaled_settings(scale_factor);
        this.scenes_formatted_choices = this.get_scenes_formatted_choices();
        this.container_padding_height = 2 * this.settings.container_padding;
        this.rows = this.get_rows_metrics();
    }

    /**
     * @returns {object} the choices panel settings object given in the instance parameters 
     * scaled by the scale_factor parameter
     */
    get_scaled_settings(scale_factor) {
        const { settings } = this.params;
        return Object.assign({ ...settings }, {
            font_size: settings.font_size * scale_factor,
            active_choice_border_width: settings.active_choice_border_width * scale_factor,
            active_choice_rounded_corners_radius: settings.active_choice_rounded_corners_radius * scale_factor,
            choice_padding: settings.choice_padding * scale_factor,
            container_padding: settings.container_padding * scale_factor,
        });
    }

    /**
     * @param {Number} row_nb Optional - 1 or 2 if we don't want to get the number of choice rows at its max
     * @param {Object[]} scene_choices Optional - The choices that we want to parse, default is all choices.
     * @returns {Object[]} An array of 2 objects like {text_height, padding_height} for each choices row
     */
    get_rows_metrics(row_nb, scene_choices) { // params will be default if undefined
        const { settings } = this;
        const max_lines_per_row = this.get_choices_max_lines_per_row(row_nb, scene_choices);

        const char_size = get_canvas_char_size(window.mentalo_drawing_context, settings);
        const text_line_h = char_size.text_line_height;
        const interline_h = char_size.interline_height;

        const choice_padding_height = 2 * settings.choice_padding;

        const height_text_row_1 = max_lines_per_row[0] * text_line_h - (max_lines_per_row[0] > 0 ? interline_h : 0);
        const height_text_row_2 = max_lines_per_row[1] > 0 // If some scene have more than 2 choices, there will be displayed on 2 rows.
            ? max_lines_per_row[1] * text_line_h - - (max_lines_per_row[1] > 0 ? interline_h : 0)
            : 0;

        return [
            {
                text_height: height_text_row_1,
                padding_height: choice_padding_height
            },
            {
                text_height: height_text_row_2,
                padding_height: height_text_row_2 > 0 ? choice_padding_height : 0
            }
        ];
    }

    /** 
     * The maximum number of text lines that each row of choice can have
     * @returns {Integer[]} An array with 2 items. 1st is for the upper choices row, second is for the lower one.
     */
    get_choices_max_lines_per_row(row_nb, scenes_choices) {
        const choices_max_row_nb = row_nb || this.get_max_choices_row_per_scene();
        const formatted_choices = scenes_choices || this.scenes_formatted_choices;

        // get the largest number of text lines per choice row
        const max_lines_per_row = [0, 0];
        Array.from({ length: choices_max_row_nb }).forEach((_row, i) => {
            const slice_index = i === 0 ? [0, 2] : [2, 4];
            const largest = formatted_choices
                .map(s_choices => Math.max(...s_choices
                    .slice(...slice_index)
                    .map(c => c.text_lines.length)
                ))
                .reduce((res, nb) => Math.max(res, nb), 0);
            max_lines_per_row[i] = largest;
        });

        return max_lines_per_row;
    }

    /**
     * @returns {Integer} The maximum number of rows that the choices_panel can have in the loaded game.
     */
    get_max_choices_row_per_scene(scenes_slice_index) {
        const scenes_choices = scenes_slice_index
            ? this.params.scenes_choices.slice(scenes_slice_index, scenes_slice_index + 1)
            : this.params.scenes_choices;

        let choices_max_row_nb = 1;
        if (Math.max(...scenes_choices.map(s => s.choices.length)) > 2) {
            choices_max_row_nb = 2
        }

        return choices_max_row_nb;
    }

    /**
     * Parses the raw text of each scene choices and returns them with an additional 
     * text_lines field with the text split into lines ready to be rendered in canvas.
     * @typedef FormattedChoice
     * @property {String[]} text_lines
     * @property {...MtlChoice} - the other MtlChoice fields
     * 
     * @returns {FormattedChoice[]}
     */
    get_scenes_formatted_choices() {
        const { scenes_choices } = this.params;
        const { settings, container_width } = this;

        const char_size = get_canvas_char_size(window.mentalo_drawing_context, settings);

        const { choice_padding } = settings;
        const error_offset = .9;
        const choice_max_width = ((container_width / 2) - (2 * choice_padding)) * error_offset;
        const max_chars_per_row = choice_max_width / char_size.width;

        return scenes_choices.map(s => s.choices.map(c => {
            const words = c.text.split(" ");
            const lines = [""];
            let line_i = 0;

            words.forEach(w => {
                if ((lines[line_i] + w).length >= max_chars_per_row) {
                    line_i++;
                    lines.push("");
                }
                lines[line_i] = `${lines[line_i]}${lines[line_i] === "" ? "" : " "}${w}`;
            });
            return Object.assign({ ...c }, { text_lines: lines });
        }));
    }

    /**
     * The total calculated height of the choices panel
     * @returns {Number}
     */
    get_total_height(rows) {
        rows = rows || this.rows;
        return rows.reduce(
            (total, row) => total + row.text_height + row.padding_height, 0
        ) + this.container_padding_height;
    }

    /**
     * A copy of this instance with values scaled by the given factor
     * @param {Number} scale_factor the scaling factor
     * @returns {ChoicesPanelMetrics}
     */
    to_scaled(scale_factor) {
        return new ChoicesPanelMetrics({ ...this.params, scale_factor });
    }

    /**
     * The choices panel bounding zone has first been calculated to the maximum, 
     * but certain scenes may need less space to display their choices.
     * This return the only necessary height for a given scene
     * @param {MtlChoice[]} scene_choices 
     * @returns {Number} A bounding box {top, left, right, bottom, width, height}
     */
    get_minimum_panel_height_for_scene(scene_index) {
        const scene_choices = this.scenes_formatted_choices.slice(scene_index, scene_index + 1);
        const choices_row_nb = scene_choices[0].length > 2 ? 2 : 1;
        const rows = this.get_rows_metrics(choices_row_nb, scene_choices);
        return this.get_total_height(rows);
    }

    /**
     * Get the metrics for a given row of choices
     * @param {Integer} scene_index 
     * @param {Number} scale_fator 
     */
    get_one_choices_row_metrics(scene_index, row_index) {
        const scene_choices = this.scenes_formatted_choices.slice(scene_index, scene_index + 1);
        const rows = this.get_rows_metrics(row_index + 1, scene_choices);
        const row = rows[row_index];
        return Object.assign(row, {
            text_height: row.text_height,
            padding_height: row.padding_height,
        });
    }
}

module.exports = ChoicesPanelMetrics;