"use strict";
/**
 * Helpers to work with color values
 */

/**
 * Get a RGB color value in String from either "rgb(x,x,x)" or #xxxxxx
 * @param {String} color A RGB color value as String in the form: #ffffff or rgb(x,x,x)
 * @returns {Uint8[]} An array of 3 values from 0 to 256 for [RED, GREEN, BLUE]
 */
function color_str_to_rgb_array(color) {
    color = color.toLowerCase(0);
    if (color.includes("rgb")) {
        return color.replace(/[a-z\(\)w]/g, "")
            .split(",")
            .slice(0, 3)
            .map(n => parseInt(n));
    } else {
        return color.replace("#", "")
            .match(/.{0,2}/g)
            .slice(0, 3)
            .map(hex => parseInt(hex, 16));
    }
}

/**
 * @param {String} color A color value (#xxxxxx or rbg(x,x,x))
 * @returns The average between each tone of the given color.
 */
function get_average_rgb_color_tone(color) {
    return color_str_to_rgb_array(color).reduce((tot, n) => tot + n / 3, 0);
}

/**
 * Gets an array of 3 8bits integers for red green and blue and and return a color value as an hexadecimal string #xxxxxx
 * @param {Uint8[]} rgb 
 * @returns {String} The hexacdecimal rgb value of the color including the hash character: #xxxxxx
 */
function rgb_array_to_hex(rgb) {
    return `#${rgb.slice(0, 3).map(n => {
        const hex = n.toString(16);
        return hex.length < 2 ? '0' + hex : hex;
    }).join('')}`;
}

/**
 * Gets an array of 4 8bits integers for red green blue and transparency channel and return a color value as an hexadecimal string #xxxxxx
 * @param {Uint8[]} rgb
 * @returns {String} The hexacdecimal rgba value of the color including the hash character: #xxxxxxxx
 */
function rgba_array_to_hex(rgba) {
    rgba = rgba.length === 4 ? rgba : rgba.concat([255])
    return `#${rgba.slice(0, 4).map(n => {
        const hex = n.toString(16);
        return hex.length < 2 ? '0' + hex : hex;
    }).join('')}`;
}

/**
 * Gets a background color as argument and return a calculated optimal foreground color.
 * For example if the background is dark the function will return a lighten version of the background.
 * @param {String} base_color A RGB color value in hexadecimal form: #ffffff
 * @returns {String} The forground color in hexadecimal string value
 */
function get_optimal_visible_foreground_color(base_color, alpha = "3f") {
    // create a 2D 1px canvas context
    const tmp_canvas = document.createElement("canvas");
    tmp_canvas.width = 1;
    tmp_canvas.height = 1;
    const ctx = tmp_canvas.getContext("2d");

    // fill it with the given color
    ctx.fillStyle = base_color;
    ctx.fillRect(0, 0, 1, 1);

    // Get either a semi-transparent black or semi-transparent white regarding the base color is above or below an avg of 127
    const superpo_color = get_average_rgb_color_tone(base_color) > 127 ? `#000000${alpha}` : `#ffffff${alpha}`;

    // Fill the pixel with the semi-transparent black or white on top of the base color
    ctx.fillStyle = superpo_color;
    ctx.fillRect(0, 0, 1, 1);

    // Return the flatten result of the superposition
    return rgb_array_to_hex(Array.from(ctx.getImageData(0, 0, 1, 1).data));
}

/**
 * @param {Uint8[]} col1 A RGBA color value as an array of 4 0 to 255 integers
 * @param {Uint8[]} col2 A RGBA color value as an array of 4 0 to 255 integers
 * @returns {Boolean} true if the 2 colors are equals
 */
function same_rgba(col1, col2) {
    return col1[0] === col2[0]
        && col1[1] === col2[1]
        && col1[2] === col2[2]
        && col1[3] === col2[3];
}

module.exports = {
    get_average_rgb_color_tone,
    get_optimal_visible_foreground_color,
    color_str_to_rgb_array,
    rgb_array_to_hex,
    rgba_array_to_hex,
    same_rgba,
};