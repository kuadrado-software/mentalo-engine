"use strict";

const Loadable = require("./loadable");

/**
 * The data type used to represent and manage the soundtrack of a scene
 */
class MtlSoundTrack extends Loadable {
    constructor(shared_state) {
        super(new Audio(), "audio", "loadeddata");
        this.name = "";
        this.shared_state = shared_state;
        this.initialized = false;
    }

    /**
     * Reset the instance to empty values.
     */
    reset() {
        this.init_loadable_element(new Audio(), "audio", "loadeddata");
        this.name = "";
        this.initialized = false;
    }

    /**
     * Populates the instance from a litteral descriptor
     * @param {Object} data 
     */
    init(data) {
        const resource = this.shared_state.resources_index.get_sound(data.name);
        this.empty = !resource
        this.audio.src = resource ? resource.src : "";
        this.name = data.name;
        this.audio.loop = data._loop;
        this.initialized = true;
    }

    /**
     * Plays the audio resource.
     */
    play() {
        this.audio.play();
    }

    /**
     * Stops the audio player.
     */
    stop() {
        this.audio.pause();
        this.audio.currentTime = 0;
    }

    /**
     * Returns the state of the inner audio element
     * @returns {Boolean}
     */
    is_playing() {
        return !this.audio.paused
    }
}

module.exports = MtlSoundTrack;