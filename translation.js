const supported_locales = ["en", "fr", "es"];

/**
 * Translations for the default error messages.
 */

const translations = {
    "Some scenes have an empty image, game cannot be loaded": {
        fr: "Certaines scènes ont une image vide, le jeu ne peut pas être chargé",
        es: "Algunas escenas tienen una imagen vacía, el juego no se puede cargar"
    },
    "Destination scene has not been set.": {
        fr: "La scène de destination n'a pas été définie.",
        es: "La escena de destino no ha sido definida.",
    },
    "Next scene has not been set.": {
        fr: "La scène suivante n'a pas été définie",
        es: "La siguiente escena no ha sido definida.",
    },
};

function get_translated(str, locale) {
    return translations[str] && locale !== "en" && supported_locales.includes(locale)
        ? translations[str][locale]
        : str;
}

module.exports = {
    get_translated,
    supported_locales,
};