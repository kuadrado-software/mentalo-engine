"use strict";
/**
 * Helpers to works with default web fonts
 */
const FONT_FAMILIES = [
    {
        category: "Sans serif", values: [
            { value: "sans-serif", text: "sans-serif" },
            { text: "Arial", value: "Arial, sans-serif" },
            { text: "Helvetica", value: "Helvetica, sans-serif" },
            { text: "Verdana", value: "Verdana, sans-serif" },
            { text: "Trebuchet MS", value: "Trebuchet MS, sans-serif" },
            { text: "Noto Sans", value: "Noto Sans, sans-serif" },
            { text: "Gill Sans", value: "Gill Sans, sans-serif" },
            { text: "Avantgarde", value: "Avantgarde, TeX Gyre Adventor, URW Gothic L, sans-serif" },
            { text: "Optima", value: "Optima, sans-serif" },
            { text: "Arial Narrow", value: "Arial Narrow, sans-serif" }
        ],
    },
    {
        category: "Serif", values: [
            { text: "serif", value: "serif" },
            { text: "Times", value: "Times, Times New Roman, serif" },
            { text: "Didot", value: "Didot, serif" },
            { text: "Georgia", value: "Georgia, serif" },
            { text: "Palatino", value: "Palatino, URW Palladio L, serif" },
            { text: "Bookman", value: "Bookman, URW Bookman L, serif" },
            { text: "New Century Schoolbook", value: "New Century Schoolbook, TeX Gyre Schola, serif" },
            { text: "American Typewriter", value: "American Typewriter, serif" }
        ],
    },
    {
        category: "Monospace", values: [
            { text: "monospace", value: "monospace" },
            { text: "Andale Mono", value: "Andale Mono, monospace" },
            { text: "Courrier New", value: "Courier New, monospace" },
            { text: "Courrier", value: "Courier, monospace" },
            { text: "FreeMono", value: "FreeMono, monospace" },
            { text: "OCR A Std", value: "OCR A Std, monospace" },
            { text: "DejaVu Sans Mono", value: "DejaVu Sans Mono, monospace" },
        ],
    },
    {
        category: "Cursive", values: [
            { text: "cursive", value: "cursive" },
            { text: "Comic Sans MS", value: "Comic Sans MS, Comic Sans, cursive" },
            { text: "Apple Chancery", value: "Apple Chancery, cursive" },
            { text: "Bradley Hand", value: "Bradley Hand, cursive" },
            { text: "Brush Script MT", value: "Brush Script MT, Brush Script Std, cursive" },
            { text: "Snell Roundhand", value: "Snell Roundhand, cursive" },
            { text: "URW Chancery L", value: "URW Chancery L, cursive" },
        ],
    }
];

const TEXT_ALIGN_OPTIONS = [
    { value: "left", text: "Left" }, // TODO trad or icon
    { value: "right", text: "Right" },
    { value: "center", text: "Center" },
];

const FONT_STYLE_OPTIONS = [
    { value: "normal", text: "Normal" },
    { value: "italic", text: "Italic" }, // TODO trad or icon
];

const FONT_WEIGHT_OPTIONS = [
    { value: "1", text: "Thin" },
    { value: "normal", text: "Normal" },
    { value: "900", text: "Bold" },
];

/**
 * Gets an object description of a font and returns it formatted as a string that can be given to a 2D drawing context.
 * Example : ctx.font = get_canvas_font({font_family:"monospace", font_size: 32})
 * @param {Object} font_data An object description of the font settings, with font_size, font_family, font_style, font_weight.s
 * @returns {String}
 */
function get_canvas_font(font_data = {}) {
    const { font_size = 20, font_family = "sans-serif", font_style = "normal", font_weight = "normal" } = font_data;
    const font_variant = "normal";
    return `${font_style} ${font_variant} ${font_weight} ${font_size.toFixed()}px ${font_family}`;
}

/**
 * @returns {Object} A map of all defined constants for font families, text align options, font styles and font weights.
 */
function get_font_options() {
    return {
        font_families: FONT_FAMILIES,
        text_align_options: TEXT_ALIGN_OPTIONS,
        font_style_options: FONT_STYLE_OPTIONS,
        font_weight_options: FONT_WEIGHT_OPTIONS,
    };
}

/**
 * An approximation of the average width and height of a character for a 2D drawing context with a given font configuration.
 * @param {CanvasRenderingContext2D} ctx
 * @returns {Object} An object with {width, height, text_line_height} entries
 */
function get_canvas_char_size(ctx, font_settings) {
    ctx.save();
    ctx.font = get_canvas_font(font_settings);

    const str = `Lorem ipsum dolor Sit amet, Consectetur adipiscing elit`;
    const width = ctx.measureText(str).width / str.length;

    // Scale width by an approximative 1.1 factor to calculate the height, 
    // for example for letters like j q p etc that have a part bellow the text base line
    const height = ctx.measureText("M").width * 1.1;
    const text_line_height = height + height / 2.5;

    ctx.restore();

    return {
        width,
        height,
        text_line_height,
        interline_height: height / 4
    }
}

module.exports = {
    get_canvas_font,
    get_font_options,
    get_canvas_char_size,
    FONT_FAMILIES,
    TEXT_ALIGN_OPTIONS,
    FONT_STYLE_OPTIONS,
    FONT_WEIGHT_OPTIONS
};