"use strict";

const { draw_text_in_bounds } = require("../../lib/text-tools");
const { draw_rect } = require("../../lib/shape-tools");
const MtlUiComponent = require("./ui-component");

/**
 * A component to display an error message in a popup
 */
class UserErrorPopup extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.state = {
            text: {},
        };

        this.set_str_id("UserErrorPopup");
    }

    /**
     * Draw the popup to the canvas.
     * Draws the box, then draw the text lines.
     * draw_children is for the closing_icon.
     */
    draw() {
        super.draw();
        const { text_lines, bounding_zone, padding, settings, modal_bounds, clear_modal_color } = this.params;
        this.clear_bounding_zone({ use_bounding_zone: modal_bounds, clear_color: clear_modal_color });

        const ctx = window.mentalo_drawing_context;
        const { left, top, width, height } = bounding_zone;

        draw_rect(ctx, left, top, width, height, {
            fill_color: settings.background_color,
            rounded_corners_radius: settings.rounded_corners_radius,
            border: {
                width: settings.border_width,
                color: settings.font_color,
            },
        });

        const complete = draw_text_in_bounds(
            ctx,
            text_lines,
            bounding_zone,
            Object.assign({ ...settings }, padding),
            this.state.text,
            !!this.params.stream_text
        );

        if (complete) {
            this.draw_children();
        }
    }
}

module.exports = UserErrorPopup;