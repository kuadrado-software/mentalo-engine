"use strict";

const MtlUiComponent = require("./ui-component");

class TextFeedbackCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("TextFeedbackCpt");
    }

    draw() {
        const text = this.params.get_text_feedback();

        if (text) {
            const ctx = window.mentalo_drawing_context;
            ctx.save();
            ctx.fillStyle = "rgba(0,0,0,.5)";
            ctx.textBaseline = "top"
            const font_size = 35;
            ctx.font = `${font_size}px Arial`;

            const txt_w = ctx.measureText(text).width;
            const margin_x = 20;
            const margin_y = 10;

            ctx.fillRect(0, 0, txt_w + margin_x * 2, font_size + 2 * margin_y);
            ctx.fillStyle = "white";
            ctx.fillText(text, margin_x, margin_y);
            ctx.restore();
        }

    }
}

module.exports = TextFeedbackCpt;