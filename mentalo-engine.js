"use strict";

const MtlGame = require("./model/game");
const SCENE_TYPES = require("./model/scene-types");
const MtlRender = require("./render/render");
const { supported_locales, get_translated } = require("./translation");

/**
 * The encapsulating class of all the components of the Mentalo game engine.
 * This is the class that should be used from outside to run a game.
 */
class MentaloEngine {
    /**
     * Initialize A MtlGame instance, populates it with the gama_data given as param.
     * Initializes the DOM element to contain the rendering of the engine.
     * Defines the listeners for Escape key and initialize a MtlRender instance.
     * @param {Object} params 
     * Parameter params.game_data is required and should be the result of the exportation format of the Mentalo editor app.
     */
    constructor(params) {
        this.game = new MtlGame();
        this.game.load_data(params.game_data);

        this.game.on_load_resources(() => this.loading = false);

        this.use_locale = params.use_locale && supported_locales.includes(params.use_locale)
            ? params.use_locale
            : "en";

        /**
         * Allow to exit game by typing Escape or q
         * @param {Event} e 
         */
        this.escape_listener = e => {
            const k = e.key.toLowerCase();
            (k === "escape" || k === "q") && this.quit();
        };

        window.addEventListener("keydown", this.escape_listener);

        this.on_quit_game = function () {
            if (this.used_default_container) {
                this.container.remove();
            }
            window.removeEventListener("keydown", this.escape_listener);
            params.on_quit_game && params.on_quit_game();
        };

        this.used_default_container = false;

        this.container = params.container || (() => {
            const el = document.createElement("div");
            el.style.display = "flex";
            el.style.justifyContent = "center";
            el.style.alignItems = "center";
            el.style.position = "fixed";
            el.style.top = 0;
            el.style.bottom = 0;
            el.style.left = 0;
            el.style.right = 0;
            el.style.zIndex = 1000;
            el.style.overflow = "hidden";
            document.body.appendChild(el);
            this.used_default_container = true
            return el;
        })();

        this.render = new MtlRender({
            container: this.container,
            fullscreen: params.fullscreen,
            frame_rate: params.frame_rate,
            get_game_settings: this.get_game_settings.bind(this),
            get_game_scenes: this.get_game_scenes.bind(this),
            get_scene: this.get_scene.bind(this),
            get_scene_index: this.get_scene_index.bind(this),
            get_inventory: this.get_inventory.bind(this),
            on_game_object_click: this.on_game_object_click.bind(this),
            on_drop_inventory_object: this.on_drop_inventory_object.bind(this),
            on_choice_click: this.on_choice_click.bind(this),
        });

        this.loading = true;
        this.will_quit = false;

        this.current_sound_track = undefined;
    }

    /**
     * Returns a fragment of the game instance ui_options, which are the styling settings of the game interface.
     * Can be the settings for text_boxes, choices_panel, inventory, etc.
     * @param {String} key The key of the wanted interface settings.
     * @returns {Object} The fragment of this.game.game_ui_options for the given key.
     */
    get_game_settings(key) {
        return this.game.game_ui_options[key];
    }

    /**
     * @returns {MtlScene[]} Returns an array of the scenes of the game.
     */
    get_game_scenes() {
        return this.game.scenes;
    }

    /**
     * @returns {MtlScene} The scene that is currently displayed
     */
    get_scene() {
        return this.game.get_scene();
    }

    /**
     * @returns {Integer} The index of the scene that is currently displayed
     */
    get_scene_index() {
        return this.game.scenes.indexOf(this.get_scene());
    }

    /**
     * Returns the game objects currently saved in the inventory.
     * @returns {MtlGameObject[]}
     */
    get_inventory() {
        return this.game.state.inventory;
    }

    /**
     * Sets the flag this.will_quit to true.
     * This flag will be parsed in the rendering loop and the engine will then effectively exit the game calling __exit().
     */
    quit() {
        this.will_quit = true;
    }

    /**
     * Clean the render instance, stops the rendering loop and calls the on_quit_game callback.
     * This is called when the rendering loop reads the this.will_quit flag as true.
     */
    __quit() {
        window.cancelAnimationFrame(window.mentalo_engine_animation_id);
        this.render.exit_fullscreen();
        this.render.clear_event_listeners();
        this.current_sound_track && this.current_sound_track.stop();
        this.on_quit_game();
    }


    /**
     * Initialize the rendering loop and the MtlRender instance.
     * Also checks that the first loaded scene has an image to load. 
     * If it doesn't the game will exit immediately in order to avoid having a black screen.
     * ( If the first scene doesn't have an image to load, the game.on_load_resource callback will 
     * never be called and the engine will remain in loading state.  )
     */
    init() {
        window.requestAnimationFrame = window.requestAnimationFrame
            || window.mozRequestAnimationFrame
            || window.webkitRequestAnimationFrame
            || window.msRequestAnimationFrame;

        window.cancelAnimationFrame = window.cancelAnimationFrame
            || window.mozCancelAnimationFrame;

        if (window.mentalo_engine_animation_id) {
            window.cancelAnimationFrame(window.mentalo_engine_animation_id);
        }

        this.render.init();

        if (this.game.scenes.find(scene => scene.animation.empty)) {
            alert(get_translated("Some scenes have an empty image, game cannot be loaded", this.use_locale));
            this.quit();
        }
    }

    /**
     * Clear the event listeners and rendering element related to the current scene from the MtlRender instance,
     * And sets the game current scene state to a new index.
     * @param {Integer} index 
     */
    go_to_scene(index) {
        this.render.reset_user_error_popup();
        this.render.clear_event_listeners();
        this.game.go_to_scene(index);
        this.render.clear_children();
        this.render.reset_text_box_visibility();
    }

    /**
     * Event listener for doubleclick on a game object.
     * Adds the object to inventory.
     * @param {MtlGameObject} obj 
     */
    on_game_object_click(obj) {
        this.game.inventory_has_empty_slot() && this.game.add_object_to_inventory(obj);
    }

    /**
     * All callback called from the inventory render component when an object image is clicked in the inventory.
     * Removes the object from inventory.
     * @param {MtlGameObject} obj 
     */
    on_drop_inventory_object(obj) {
        this.game.remove_object_from_inventory(obj);
    }

    /**
     * Callback called from the choices_panel render component.
     * Handles the click on a choice.
     * @param {MtlChoice} choice 
     * @returns 
     */
    on_choice_click(choice) {
        const { destination_scene_index, use_objects } = choice;
        if (destination_scene_index === -1) { // -1 is default index when the choice destination scene is not set.
            this.render.set_user_error_popup({
                text: get_translated("Destination scene has not been set.", this.use_locale),
            });
            return;
        }

        // Handle the case where the clicked choice requires some objects to be presents in the inventory.
        if (use_objects.value) {
            const inventory = this.get_inventory();
            const objs = [];
            for (const it of use_objects.items) {
                const ob = Array.from(inventory).find(o => o.name === it.name);
                if (ob) {
                    objs.push(ob);
                } else {
                    this.render.set_user_error_popup({
                        text: use_objects.missing_object_message, stream_text: true,
                    });
                    return;
                }
            }

            objs.forEach(o => use_objects.items
                .find(it => o.name === it.name)
                .consume_object && this.game.consume_game_object(o));
        }

        if (destination_scene_index === -2) { // -2 is the index used to indicate that destination scene is just the end of program.
            this.quit()
            return;
        }

        this.go_to_scene(destination_scene_index);
    }

    /**
     * Callback called when a scene of type "Cinematic" reaches end.
     */
    on_cinematic_end() {
        const scene = this.game.get_scene();
        if (scene.end_cinematic_options.quit) {
            this.quit();
        } else if (scene.end_cinematic_options.destination_scene_index === -1) {
            this.render.set_user_error_popup({
                text: get_translated("Next scene has not been set.", this.use_locale),
                on_close: this.quit.bind(this),
            });
        } else {
            this.go_to_scene(scene.end_cinematic_options.destination_scene_index);
        }
    }

    update_soundtrack() {
        const sound_track = this.game.get_soundtrack();
        if (!this.current_sound_track || sound_track.name !== this.current_sound_track.name) {

            if (this.current_sound_track && this.current_sound_track.is_playing()) {
                this.current_sound_track.stop();
            }

            this.current_sound_track = sound_track;
        }

        if (this.current_sound_track && this.current_sound_track.loaded && !this.current_sound_track.is_playing()) {
            this.current_sound_track.play();
        }
    }

    /**
     * The rendering loop.
     * Handles the different states of the engine (quit, loading or runnning normally)
     * @returns The id of the registered window.requestAnimationFrame
     */
    run_game() {
        if (this.will_quit) {
            this.__quit();
            return;
        } else if (this.loading) {
            this.render.draw_loading();
        } else {
            if (this.get_scene()._type === SCENE_TYPES.CINEMATIC) {
                this.game.update_cinematic_timeout();
                if (this.game.is_cinematic_ended()) {
                    this.on_cinematic_end();
                }
            }

            this.update_soundtrack();

            this.render.draw_game();
        }

        window.mentalo_engine_animation_id = requestAnimationFrame(this.run_game.bind(this));
    }
}

module.exports = MentaloEngine;