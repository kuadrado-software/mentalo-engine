/**
 * An enum like object to describe the 2 possible type that can have a MtlScene
 */

module.exports = {
    PLAYABLE: "Playable",
    CINEMATIC: "Cinematic",
};