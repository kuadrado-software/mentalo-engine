"use strict";

const MtlUiComponent = require("./ui-component");

/**
 * The component that draws the scene animation.
 */
class SceneAnimationCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("SceneAnimationCpt");
        this.framecount = 0;
        // This state controls the visibility of the text box child component.
        this.state.text_box_visible = true;
    }

    /**
     * Sets the text_box_visibility state.
     * @param {Boolean} value 
     */
    set_text_box_visibility(value) {
        this.state.text_box_visible = value;
    }

    /**
     * Increments the animation frame count
     */
    update_framecount() {
        this.framecount = this.framecount + 1 <= Number.MAX_SAFE_INTEGER ? this.framecount + 1 : 0;
    }

    /**
     * Draw the scene animation on the canvas  at the frame given by the animation state
     * and draw each child component like text box, game objects and error popup.
     */
    draw() {
        super.draw();
        const { bounding_zone, get_animation, next_frame_ready } = this.params;
        if (next_frame_ready()) {
            this.clear_bounding_zone();
            const ctx = window.mentalo_drawing_context;

            const animation = get_animation();
            animation.update_frame(this.framecount);
            const dim = animation.dimensions;
            const w = dim.width;
            const h = dim.height;
            const offsetX = animation.frame * w;

            const cw = bounding_zone.width;
            const ch = bounding_zone.height;

            // center the image
            if (!animation.canvas_precalc) {
                animation.canvas_precalc = {
                    dx: bounding_zone.left,
                    dy: bounding_zone.top,
                    dw: cw,
                    dh: ch,
                };

                if (w / h > cw / ch) {
                    // image is more panoramic than canvas
                    animation.canvas_precalc.dw = cw;
                    animation.canvas_precalc.dh = cw * (h / w);
                    // center vertically
                    animation.canvas_precalc.dy = bounding_zone.top + ((ch - animation.canvas_precalc.dh) / 2);
                } else if (cw / ch > w / h) {
                    // canvas is more panoramic
                    animation.canvas_precalc.dh = ch;
                    animation.canvas_precalc.dw = ch * (w / h);
                    // center horizontally
                    animation.canvas_precalc.dx = bounding_zone.left + ((cw - animation.canvas_precalc.dw) / 2);
                } else {
                    // same ratio
                    animation.canvas_precalc.dh = ch;
                    animation.canvas_precalc.dw = cw;
                }
            }

            const { dx, dy, dw, dh } = animation.canvas_precalc;
            ctx.drawImage(
                animation.image,
                offsetX, 0,
                w, h,
                dx, dy,
                dw, dh
            );

            this.draw_children();
            this.update_framecount();
        }
    }
}

module.exports = SceneAnimationCpt;