"use strict";

/**
 * Draws a rectangle on a canvas 2D context with support of corner rounding and border options.
 * @param {CanvasRenderingContext2D} ctx
 * @param {Integer} x 
 * @param {Integer} y 
 * @param {Integer} width 
 * @param {Integer} height 
 * @param {Object} options 
 */
function draw_rect(ctx, x, y, width, height, options = {
    rounded_corners_radius: 0,
    border: {
        width: 0,
        color: "rgba(0,0,0,0)"
    },
    fill_color: "black",
}) {
    const { rounded_corners_radius = 0, border = { width: 0, color: "rgba(0,0,0,0)" }, fill_color = "black", fill_image } = options;
    const smallest_axis = Math.min(width, height);
    const radius = rounded_corners_radius > smallest_axis / 2 ? smallest_axis / 2 : rounded_corners_radius;

    ctx.save();

    ctx.beginPath();
    ctx.arc(x + radius, y + radius, radius, Math.PI, 3 * Math.PI / 2);
    ctx.lineTo(x + width - radius, y);
    ctx.arc(x + width - radius, y + radius, radius, 3 * Math.PI / 2, 0);
    ctx.lineTo(x + width, y + height - radius);
    ctx.arc(x + width - radius, y + height - radius, radius, 0, Math.PI / 2);
    ctx.lineTo(x + radius, y + height);
    ctx.arc(x + radius, y + height - radius, radius, Math.PI / 2, Math.PI);
    ctx.closePath();

    ctx.clip();

    if (fill_image) {
        ctx.drawImage(
            fill_image.src,
            0, 0,
            fill_image.src.width, fill_image.src.height,
            fill_image.dx, fill_image.dy,
            fill_image.dw, fill_image.dh,
        )
    } else {
        ctx.fillStyle = fill_color;
        ctx.fillRect(x, y, width, height);
    }

    if (border.width > 0) {
        ctx.strokeStyle = border.color;
        ctx.lineWidth = border.width;
        ctx.stroke();
    }

    ctx.restore();
}

module.exports = {
    draw_rect,
}