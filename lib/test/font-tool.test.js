const { get_canvas_char_size, get_canvas_font } = require("../font-tools");
const createTestingContext = require("../../test/create-testing-context");

createTestingContext();

test("get canvas font", () => {
    expect(get_canvas_font({
        font_size: 32,
        font_family: "monospace",
        font_style: "italic",
        font_weight: "bold"
    })).toBe("italic normal bold 32px monospace");

    expect(get_canvas_font({
        font_size: 14,
        font_family: "Times",
        font_style: "normal",
        font_weight: "normal"
    })).toBe("normal normal normal 14px Times");
});