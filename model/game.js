"use strict";

const MtlResourcesIndex = require("./resources_index");
const MtlScene = require("./scene");
const SCENE_TYPES = require("./scene-types");

/**
 * The data type to encapsulate a loaded game.
 */
class MtlGame {
    /**
     * Initialize the instance with default data.
     */
    constructor() {
        this.name = "";

        this.resources_index = new MtlResourcesIndex()

        this.scenes = [new MtlScene({ resources_index: this.resources_index })];

        // Game interface styling preferences
        this.game_ui_options = {
            // The overall look of the innterface
            general: {
                background_color: "#000000",
                animation_canvas_dimensions: {
                    width: 600,
                    ratio: "4:3",
                    height: function () {
                        const split_ratio = this.ratio.split(":").map(n => parseInt(n));
                        const factor = split_ratio[1] / split_ratio[0];
                        return Number((this.width * factor).toFixed());
                    },
                },
            },
            // The text box displayed on top of a scene image if a message is defined
            text_boxes: {
                background_color: "#222222",
                font_size: 20,
                font_style: "normal",
                font_weight: "normal",
                font_family: "Arial, sans-serif",
                font_color: "#ffffff",
                text_align: "left",
                padding: 20,
                margin: 20,
                border_width: 0,
                rounded_corners_radius: 0,
            },
            // The panel containing the choices buttons below the scene image.
            choices_panel: {
                background_color: "#222222",
                font_size: 20,
                font_family: "sans",
                font_color: "#ffffff",
                font_style: "normal",
                text_align: "left",
                font_weight: "normal",
                container_padding: 20,
                choice_padding: 10,
                active_choice_background_color: "rgba(255,255,255,.4)",
                active_choice_border_width: 0,
                active_choice_rounded_corners_radius: 0,
            },
            // the inventory grid drawn at the right of the scene image
            inventory: {
                background_color: "#222222",
                columns: 1,
                rows: 4,
                gap: 10,
                padding: 20,
                slot_rounded_corner_radius: 0,
                slot_border_width: 2,
            },
        };

        this.starting_scene_index = 0;

        // The state of the running game
        this.state = {
            scene: this.starting_scene_index,
            inventory: new Set(),
            cinematic_timeout: {
                inc: 0,
                last_update_time: -1,
                timeout: false,
            },
        };

        // This state will be incremented for each when it finishes to load its image and sound resources.
        // The loaded_scenes value must be equal to the scenes number for the game to consider that all resources are loaded.
        this.load_state = {
            loaded_scenes: 0,
        };
    }

    /**
     * Returns the scene that is currently displayed
     * @returns {MtlScene}
     */
    get_scene() {
        return this.scenes[this.state.scene];
    }

    /**
     * @returns {MtlSoundTrack} the soundtrack that is set for the current scene
     */
    get_soundtrack() {
        return this.get_scene().sound_track;
    }

    /**
     * Updates the state with a new scene index.
     * @param {Integer} index The index of the wanted scene
     */
    go_to_scene(index) {
        const current_scene = this.get_scene();
        current_scene.animation.reset_frame();
        if (current_scene._type == SCENE_TYPES.CINEMATIC) {
            this.state.cinematic_timeout = {
                inc: 0,
                last_update_time: -1,
                timeout: false,
            };
        }
        this.state.scene = index;
    }

    /**
     * This is called if the current scene is of type Cinematic.
     * UIncrements the cinematic_timeout counter and update the 
     * cinematic_timeout.timeout state to true if the cinematic 
     * has been running as much or more time than what is defined in scene.cinematic_duration.
     */
    update_cinematic_timeout() {
        if (this.state.cinematic_timeout.timeout) return;

        const t = new Date().getTime();

        if (this.state.cinematic_timeout.last_update_time === -1) {
            this.state.cinematic_timeout.last_update_time = t;
        }

        this.state.cinematic_timeout.inc = t - this.state.cinematic_timeout.last_update_time;
        this.state.cinematic_timeout.timeout = this.get_scene().cinematic_duration * 1000 <= this.state.cinematic_timeout.inc;
    }

    /**
     * Returns a state wether the running cinematic is ended or not
     * @returns {Boolean}
     */
    is_cinematic_ended() {
        const scene = this.get_scene();
        if (scene.cinematic_duration === 0) return scene.animation.finished;
        return this.state.cinematic_timeout.timeout;
    }

    /**
     * Takes a game object as parameter and references it in the inventory state.
     * @param {MtlGameObject} obj 
     */
    add_object_to_inventory(obj) {
        this.state.inventory.add(obj);
    }

    /**
     * @returns {Boolean} true if inventory has an empty slot.
     */
    inventory_has_empty_slot() {
        const { columns, rows } = this.game_ui_options.inventory;
        return this.state.inventory.size < columns * rows;
    }

    /**
     * Take a game object as argument and removes its reference from the inventory state.
     * @param {MtlGameObject} obj 
     */
    remove_object_from_inventory(obj) {
        this.state.inventory.delete(obj);
    }

    /**
     * Takes a game object as argument, removes the reference from inventory 7
     * and delete the object from the scene it's defined into.
     * @param {MtlGameObject} obj 
     */
    consume_game_object(obj) {
        this.remove_object_from_inventory(obj);
        for (const s of this.scenes) {
            const found_obj = s.game_objects.find(o => o === obj);
            if (found_obj) {
                s.game_objects.splice(s.game_objects.indexOf(found_obj), 1);
                break;
            }
        }
    }

    /**
     * Returns true if the number of scenes having finished to load their loadable resources is equal to the total number of scenes.
     * @returns {Boolean}
     */
    all_resources_loaded() {
        return this.load_state.loadable_elements === this.load_state.loaded_elements;
    }

    /**
     * Returns a concatenation of all game objects of all scenes.
     * @returns {MtlGameObject[]}
     */
    get_game_objects() {
        return this.scenes.reduce((acc, scene) => acc.concat(scene.game_objects), []);
    }

    /**
     * Populates the instance with a litteral game desriptor.
     * The expected data format is identical to what's exported from the Mentalo app editor,
     * or what's returned from the Mentalo API database.
     * @param {Object} data 
     */
    load_data(data) {
        this.name = data.name;

        this.starting_scene_index = data.starting_scene_index || 0;
        this.state.scene = this.starting_scene_index;

        this.resources_index.load_data(data.resources_index);

        this.scenes = data.scenes.map(scene => {
            const scene_instance = new MtlScene({ resources_index: this.resources_index });
            scene_instance.load_data(scene);
            scene_instance.on_load_group_complete(() => {
                this.load_state.loaded_scenes++;
                if (this.load_state.loaded_scenes === this.scenes.length) {
                    this.on_load_resources_callback && this.on_load_resources_callback();
                }
            })
            return scene_instance;
        });

        this.game_ui_options = Object.assign(data.game_ui_options, {
            general: {
                background_color: data.game_ui_options.general.background_color,
                animation_canvas_dimensions: Object.assign(data.game_ui_options.general.animation_canvas_dimensions, {
                    height: function () {
                        const split_ratio = this.ratio.split(":").map(n => parseInt(n));
                        const factor = split_ratio[1] / split_ratio[0];
                        return Number((this.width * factor).toFixed());
                    },
                }),
            }
        });
    }

    /**
     * Sets the callback to call when all resources of all scenes are fully loaded.
     * @param {Function} callback 
     */
    on_load_resources(callback) {
        this.on_load_resources_callback = callback;
    }
}

module.exports = MtlGame;