"use strict";

const { draw_rect } = require("../../lib/shape-tools");
const MtlUiComponent = require("./ui-component");

/**
 * The component to draw a grid slot in the inventory panel.
 */
class InventorySlotCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("InventorySlotCpt");
    }

    /**
     * Draws a rectangle as the grid slot.
     * Calls draw_children to draw the image of the game object stored in that slot if there is one.
     */
    draw() {
        super.draw();
        const { bounding_zone, stroke_color, settings } = this.params;
        const { left, top, width, height } = bounding_zone;
        const ctx = window.mentalo_drawing_context;

        draw_rect(ctx, left, top, width, height, {
            fill_color: "rgba(0,0,0,0)",
            rounded_corners_radius: settings.slot_rounded_corner_radius,
            border: {
                width: settings.slot_border_width,
                color: stroke_color,
            }
        });

        this.draw_children();
    }
}

module.exports = InventorySlotCpt;