"use strict";

const MtlUiComponent = require("./ui-component");

/**
 * A rendering component that displays a closing icon (a cross inside a circle)
 */
class ClosingIconCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("ClosingIconCpt")
    }

    /**
     * Uses standard vectorial drawing methods to draw a circle and a cross at 
     * given coordinates on the registered 2D drawing context.
     */
    draw() {
        super.draw();
        this.clear_bounding_zone();
        const { color, center, radius = 5, line_width = 2, background_color = "rgba(0,0,0,0)" } = this.params;
        const ctx = window.mentalo_drawing_context;
        const padding = radius / 1.5;

        const cross_coords = {
            left: center.x - radius + padding,
            right: center.x + radius - padding,
            top: center.y - radius + padding,
            bottom: center.y + radius - padding,
        };

        ctx.fillStyle = background_color;
        ctx.beginPath();
        ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI);
        ctx.fill();

        ctx.strokeStyle = color;
        ctx.lineWidth = line_width;
        ctx.beginPath();
        ctx.arc(center.x, center.y, radius, 0, 2 * Math.PI);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(cross_coords.left, cross_coords.top);
        ctx.lineTo(cross_coords.right, cross_coords.bottom);
        ctx.moveTo(cross_coords.left, cross_coords.bottom)
        ctx.lineTo(cross_coords.right, cross_coords.top);
        ctx.stroke();
    }
}

module.exports = ClosingIconCpt;