"use strict";

/**
 * An abstract type that must be inherited by any object that have a resource to load.
 * Can be an image or a sound.
 */
class Loadable {
    /**
     * Initialization of the actual loadable element.
     * @param {HTMLElement} loadable_element Image() or Audio() for example
     * @param {String} field_name the name of the field that references the loadable element in the instance.
     * @param {String} event_name the name of the event that is dispatched by the loadable 
     *                            html element when it has loaded a resource ("load" or "loadeddata")
     */
    constructor(loadable_element, field_name, event_name) {
        this.loaded = false;
        this.init_loadable_element(loadable_element, field_name, event_name);
    }

    /**
     * Initializes event listeners for the registered loadable_element.
     * @param {HTMLElement} loadable_element 
     * @param {String} field_name the name of the field that references the element
     * @param {String} event_name the event to use for resource loading (images uses onload, audio uses onloadeddata ...)
     */
    init_loadable_element(loadable_element, field_name, event_name) {
        this[field_name] = loadable_element;

        this.load_listener = () => {
            this.loaded = true;
            this.on_load_callback();
            this.clear();
        };

        this[field_name].addEventListener(event_name, this.load_listener);

        this.clear = () => {
            this[field_name].removeEventListener(event_name, this.load_listener);
        }
    }

    /**
     * Sets a callback to call when the loadable_element has finished to load its resource.
     * @param {Function} callback 
     */
    on_load(callback) {
        this.on_load_custom_callback = callback;
    }

    /**
     * The callback that's called when the loadable element as finished loading its resource
     */
    on_load_callback() {
        this.loaded = true;
        this.on_load_custom_callback && this.on_load_custom_callback();
    }
}

module.exports = Loadable;