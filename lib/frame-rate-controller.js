"use strict";

/**
 * A little helper class to control the rendering frame rate
 */
class FrameRateController {
    /**
     * @param {Integer} fps The wanted frame rate in frame per second
     */
    constructor(fps) {
        this.tframe = performance.now();
        this.interval = 1000 / fps; // convert in milliseconds
        this.initial = true;
    }

    /**
     * @returns {Boolean} true if the defined interval is elapsed.
     */
    nextFrameReady() {
        if (this.initial) {
            this.initial = false;
            return true;
        }

        const now = performance.now();
        const elapsed = now - this.tframe;
        const ready = elapsed > this.interval;

        if (ready) {
            this.tframe = now - (elapsed % this.interval);
        }

        return ready;
    }
}

module.exports = FrameRateController;