"use strict";

const { draw_rect } = require("../../lib/shape-tools");
const MtlUiComponent = require("./ui-component");

/**
 * The component to display a game object image inside a grid slot of the inventory panel.
 */
class InventoryObjectCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("InventoryObjectCpt")
    }

    /**
     * If a game object exists for the slot bound to this instance, its image will be drawn cropped inside a bounding rectangle.
     * When a game object is hovered in the inventory, the draw_inventory_closing_icon is updated and a red cross will be drawn 
     * on top of the object image. If the cross get's clicked the object is removed from inventory.
     */
    draw() {
        super.draw();
        const { get_game_object, slot_index, settings, bounding_zone, stroke_color } = this.params;
        const game_object = get_game_object(slot_index);
        if (game_object) {
            const ctx = window.mentalo_drawing_context;

            const image = game_object.ref.image;
            draw_rect(ctx, bounding_zone.left, bounding_zone.top, bounding_zone.width, bounding_zone.height, {
                fill_image: {
                    src: image,
                    dw: game_object.dimensions.width,
                    dh: game_object.dimensions.height,
                    dx: game_object.position.x,
                    dy: game_object.position.y,
                },
                rounded_corners_radius: settings.slot_rounded_corner_radius,
                border: {
                    width: settings.slot_border_width,
                    color: stroke_color,
                },
            });

            if (this.state.draw_inventory_close_icon) {
                this.draw_children(); // drop object icon
            }
        }
    }
}

module.exports = InventoryObjectCpt;