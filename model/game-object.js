"use strict";
const Loadable = require("./loadable");

/**
 * The data type used for the game_objects field of a MtlScene
 */
class MtlGameObject extends Loadable {
    /**
     * Initializes the instance with an empty image and zero position.
     */
    constructor(shared_state) {
        super(new Image(), "image", "load");
        this.shared_state = shared_state;
        this.name = "";
        this.position = { x: 0, y: 0 };
        this.state = {};
    }

    /**
     * Populates the instance with a litteral description data object
     * @param {Object} data 
     */
    load_data(data) {
        const img_resource = this.shared_state.resources_index.get_image(data.image);
        this.image.src = img_resource ? img_resource.src : "";
        this.image.name = img_resource ? data.image : "";
        this.name = data.name;
        this.position = data.position;
    }

    /**
     * @returns {Object} A {width, height} object for the dimensions of the image.
     */
    get_dimensions() {
        return {
            width: this.image.width,
            height: this.image.height,
        }
    }

    /**
     * Returns the bounding box coordinates of the object image as a {top, right, bottom, left} object.
     * @returns {Object} 
     */
    get_bounds() {
        const dims = this.get_dimensions();
        return {
            top: this.position.y,
            right: this.position.x + dims.width,
            bottom: this.position.y + dims.height,
            left: this.position.x,
        }
    }
}

module.exports = MtlGameObject;