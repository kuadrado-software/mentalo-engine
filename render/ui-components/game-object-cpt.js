"use strict";

const MtlUiComponent = require("./ui-component");

/**
 * A component that draws a GameObject
 */
class GameObjectCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("GameObjectCpt")
    }

    /**
     * Draws the image of the object on the canvas.
     */
    draw() {
        super.draw();
        const { position, dimensions, image, is_in_inventory } = this.params;
        if (is_in_inventory()) {
            return;
        }

        const ctx = window.mentalo_drawing_context;

        ctx.drawImage(
            image,
            0, 0,
            image.width, image.height,
            position.x, position.y,
            dimensions.w, dimensions.h,
        );

        if (this.state.draw_border) {
            ctx.lineWidth = 1;
            ctx.strokeStyle = "rgba(180, 180, 180, 0.5)";
            ctx.strokeRect(position.x - 5, position.y - 5, dimensions.w + 10, dimensions.h + 10);
        }
    }
}

module.exports = GameObjectCpt;