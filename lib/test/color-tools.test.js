const createTestingContext = require("../../test/create-testing-context");
const {
    get_average_rgb_color_tone,
    get_optimal_visible_foreground_color,
    color_str_to_rgb_array,
    rgb_array_to_hex,
    rgba_array_to_hex,
    same_rgba
} = require("../color-tools");

createTestingContext();

test("color_str_to_rgb_array", () => {
    const str_yellow_rgb = "rgb(255,255,0)";
    const str_red_rgb = "rgb(255,0,0)";
    const str_grey_rgb = "rgb(127,127,127)";
    const str_red_rgba = "rgba(255,0,0,.5)";
    const str_yellow_hex = "#ffff00";
    const str_red_hex = "#ff0000";
    const str_grey_hex = "#7f7f7f";
    const str_red_hex_alpha = "#ff000055";
    expect(color_str_to_rgb_array(str_yellow_rgb)).toEqual([255, 255, 0]);
    expect(color_str_to_rgb_array(str_red_rgb)).toEqual([255, 0, 0]);
    expect(color_str_to_rgb_array(str_grey_rgb)).toEqual([127, 127, 127]);
    expect(color_str_to_rgb_array(str_red_rgba)).toEqual([255, 0, 0]);
    expect(color_str_to_rgb_array(str_yellow_hex)).toEqual([255, 255, 0]);
    expect(color_str_to_rgb_array(str_red_hex)).toEqual([255, 0, 0]);
    expect(color_str_to_rgb_array(str_grey_hex)).toEqual([127, 127, 127]);
    expect(color_str_to_rgb_array(str_red_hex_alpha)).toEqual([255, 0, 0]);
});

test("get_average_rgb_color_tone", () => {
    const str_rgb = "rgb(54,255,125)";
    const str_hex = "#5f8c9a";
    expect(get_average_rgb_color_tone(str_rgb)).toBe((54 + 255 + 125) / 3);
    expect(get_average_rgb_color_tone(str_hex)).toEqual(
        (parseInt("5f", 16) + parseInt("8c", 16) + parseInt("9a", 16)) / 3
    );
});

test("rgb_array_to_hex", () => {
    const color_1 = [127, 255, 58];
    const color_2 = [0, 255, 87, 120]; // alpha channel should not be read
    expect(rgb_array_to_hex(color_1)).toBe("#7fff3a");
    expect(rgb_array_to_hex(color_2)).toBe("#00ff57");
});

test("rgba_array_to_hex", () => {
    const color_1 = [127, 255, 58]; // alpha channel should be automatically added wih value 255
    const color_2 = [0, 255, 87, 120];
    expect(rgba_array_to_hex(color_1)).toBe("#7fff3aff");
    expect(rgba_array_to_hex(color_2)).toBe("#00ff5778");
});

test("get_optimal_visible_foreground_color", () => {
    const alpha = "3f";
    const white = "#ffffff";
    const black = "#000000";
    const hex_white_superpo_value = Array.from({ length: 3 }).map(() => (255 - parseInt(alpha, 16)).toString(16)).join('');
    const hex_black_superpo_value = Array.from({ length: 3 }).map(() => alpha).join('');
    expect(get_optimal_visible_foreground_color(white, alpha)).toBe(`#${hex_white_superpo_value}`);
    expect(get_optimal_visible_foreground_color(black, alpha)).toBe(`#${hex_black_superpo_value}`);
});

test("same_rgba", () => {
    const col1 = [45, 127, 240, 58];
    const col2 = [45, 127, 240, 58];
    const col3 = [45, 127, 250];
    const col4 = [127, 255, 0];
    expect(same_rgba(col1, col2)).toBe(true);
    expect(same_rgba(col1, col3)).toBe(false);
    expect(same_rgba(col1, col4)).toBe(false);
});