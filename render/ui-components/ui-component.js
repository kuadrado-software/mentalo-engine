"use strict";

const { draw_rect } = require("../../lib/shape-tools");

// const { draw_rect } = require("../lib/shape-tools");

/**
 * A generic class that must be extended by all components registered in MtlRender.components.
 * It provides helping methods to handle event listeners and children components.
 */
class MtlUiComponent {
    constructor(params) {
        this.set_str_id();
        this.params = params;
        this.params.event_listeners = this.params.event_listeners || [];
        this.params.get_children = this.params.get_children || (() => []);
        this.params.is_visible = this.params.is_visible || (() => true);
        this.children = [];
        this.children_set = false;
        this.set_children();
        this.state = {
            event_listeners_initialized: false,
        };

        // A higher z_index will make the component render after the sibling components (in same children array) with a lower index.
        this.z_index = this.params.z_index || 1;

        this.init_event_listeners();
    }

    /**
     * Provide a identifier for the object. Default is contructor name.
     * Should be called from child class to set a custom id.
     * @param {String} str 
     */
    set_str_id(str = this.constructor.name) {
        this.str_id = str;
    }

    /**
     * Draws a black rectangle on all the surface of the bounding zone defined for this component
     * @param {Object} options can provide a clear_color<String> rgba value
     */
    clear_bounding_zone(options = {}) {
        let { bounding_zone } = this.params;
        const { use_bounding_zone } = options;
        bounding_zone = use_bounding_zone || bounding_zone;
        const { left, top, width, height } = bounding_zone;
        draw_rect(
            window.mentalo_drawing_context,
            left, top, width, height,
            {
                fill_color: options.clear_color || bounding_zone.clear_color || "rgba(0,0,0,0)"
            }
        );
    }

    /**
     * Removes the event listeners attached to this component and to children components.
     * @param {Object} options can pass a recursive flag wether children must be cleared or not. Default is true.
     */
    clear_event_listeners(options = { recursive: true }) {
        this.params.event_listeners.forEach(ref => {
            window.removeEventListener(ref.event_type, ref.listener);
        });

        if (options.recursive) {
            this.children.forEach(child => {
                child.clear_event_listeners();
            });
        }

        this.state.event_listeners_initialized = false;
    }

    /**
     * Removes children components from this component.
     * Options can define some component to exlude identifying them by their str_id.
     * The excluded component will be kept as persistent components.
     * @param {Object} options 
     */
    clear_children(options) {
        const { exclude } = options;
        const persistent_children = [];

        this.children.forEach(child => {
            if (exclude.includes(child.str_id)) {
                persistent_children.push(child);
            }
        });

        this.children = persistent_children;
        this.children_set = false;
    }

    /**
     * Register an array of children components in this component.
     * This is called when chlidren_set is false. Which happend at initialization and when clear_children is called.
     * Doing this allows saving the get_children() calculation at each frame.
     * This children components are concatenated recursively with their own children components.
     */
    set_children() {
        this.children = this.children
            .concat(this.params.get_children()
                .filter(child => {
                    // If this child has been kept as a persistent component, keep it as it.
                    const keep_previous_child = this.children.find(ch => ch.str_id === child.str_id);
                    return !keep_previous_child;
                }))
            .sort((a, b) => a.z_index > b.z_index);

        this.children_set = true;
    }

    /**
     * Attaches a event listener to this component
     * @param {Object} obj A descriptor of the event listener like :
     * {
     *      event_type: "click",
     *      listener: e => {
     *          ... something to do on click that component
     *      }
     * }
     */
    add_event_listener(obj) {
        const len = this.params.event_listeners.push(obj);
        const ref = this.params.event_listeners[len - 1];
        window.addEventListener(ref.event_type, ref.listener);
    }

    /**
     * Attaches the event listeners given as parameter to the component
     */
    init_event_listeners() {
        const { event_listeners } = this.params;
        event_listeners.forEach(obj => this.add_event_listener(obj));
        this.state.event_listeners_initialized = true
    }

    /**
     * Call the draw method of the children component
     */
    draw_children() {
        if (!this.children_set) {
            this.set_children();
        }
        this.children.forEach(c => c.draw());
    }

    /**
     * Initializes the event listeners if it's not already done.
     * This must be called by the inherited call draw method.
     */
    draw() {
        if (!this.state.event_listeners_initialized) {
            this.init_event_listeners();
        }
    }
}

module.exports = MtlUiComponent;