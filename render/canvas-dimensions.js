"use strict";

const ChoicesPanelMetrics = require("./choices-panel-metrics");

class CanvasDimensions {
    constructor(params) {
        this.params = params;

        // The original dimensions of the image part of the canvas
        this.image = { width: 0, height: 0 };

        // The scaled dimensions of the rendering canvas
        this.width = 0;
        this.height = 0;

        // Scaled dimensions of the inventory zone
        this.inventory = { width: 0, height: 0 };

        // Scaled dimensions of the choices zone
        this.choices_panel = { width: 0, height: 0 };

        // The scale factor that will have been calculated
        this.scale_factor = 1;

        this.init_dimensions();
    }

    /**
     * Calculate the dimensions from game settings
     */
    init_dimensions() {
        const { get_game_settings } = this.params;

        const general_settings = get_game_settings("general");

        const image_h = general_settings.animation_canvas_dimensions.height();
        const image_w = general_settings.animation_canvas_dimensions.width;
        const inventory_w = this.get_inventory_base_width();
        const base_w = image_w + inventory_w;

        this.choices_panel_metrics = this.get_choices_panel_metrics();

        const max_playground_width = window.screen.width - 200;
        const max_playground_height = window.screen.height - 200;

        const base_h = image_h + this.choices_panel_metrics.get_total_height();
        const playground_ratio = base_w / base_h;

        const screen_ratio = window.screen.width / window.screen.height;

        // Calculate the width of the canvas to the maximum possible scale
        if (screen_ratio > playground_ratio) {
            // Screen is more panoramic than game canvas so we scale canvas to the maximum height
            const scaled_w = max_playground_height * playground_ratio
            this.width = scaled_w <= max_playground_width ? scaled_w : max_playground_width;
        } else {
            // Image is more panoramic so its scaled to the maximum width
            const scaled_h = max_playground_width * (base_h / base_w)
            this.height = scaled_h <= max_playground_height ? scaled_h : max_playground_height;
            this.width = this.height * playground_ratio;
        }

        this.scale_factor = this.width / base_w;

        this.choices_panel_metrics = this.choices_panel_metrics.to_scaled(this.scale_factor);

        this.image = {
            width: image_w * this.scale_factor,
            height: image_h * this.scale_factor,
        };

        this.inventory = {
            width: inventory_w * this.scale_factor,
            height: this.image.height,
        };

        this.choices_panel = {
            width: this.width,
            height: this.choices_panel_metrics.get_total_height(),
        };

        this.height = this.image.height + this.choices_panel.height;
    }

    /**
     * The width of the inventory panel
     * @returns {Integer}
     */
    get_inventory_base_width() {
        const { get_game_settings } = this.params;
        const image_h = get_game_settings("general").animation_canvas_dimensions.height();
        const inventory_style = get_game_settings("inventory");
        const gap = inventory_style.gap;
        const h = image_h - (2 * inventory_style.padding);
        const gap_h = (inventory_style.rows - 1) * gap;
        const gap_w = (inventory_style.columns - 1) * gap
        const slot_side = (h - gap_h) / inventory_style.rows;
        return (inventory_style.columns * slot_side)
            + (2 * inventory_style.padding)
            + gap_w;
    }

    /**
     * The metrics in pixels for each row of choices in the choices panel
     * @returns {ChoicesPanelMetrics}
     */
    get_choices_panel_metrics() {
        const { get_game_settings, get_game_scenes } = this.params;
        const choices_panel_settings = get_game_settings("choices_panel");
        const general_settings = get_game_settings("general");

        const container_padding = choices_panel_settings.container_padding;
        const container_width = general_settings.animation_canvas_dimensions.width
            + this.get_inventory_base_width()
            - (2 * container_padding);

        const scenes_choices = get_game_scenes().map(s => { return { choices: s.choices } });

        return new ChoicesPanelMetrics({
            settings: choices_panel_settings,
            container_width,
            scenes_choices,
        });
    }
}

module.exports = CanvasDimensions;