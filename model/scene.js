"use strict";

const SCENE_TYPES = require("./scene-types");
const MtlAnimation = require("./animation");
const MtlSoundTrack = require("./sound-track");
const MtlChoice = require("./choice");
const MtlGameObject = require("./game-object");
const LoadableGroup = require("./loadable-group");

/**
 * The type used for the scenes of a MtlGame.
 * Extends the LoadableGroup class because it manages multiple objects of the class Loadable.
 */
class MtlScene extends LoadableGroup {
    /**
     * Initializes the instance with default empty data.
     */
    constructor(shared_state) {
        super();

        this.shared_state = shared_state;

        const default_data = {
            name: "",
            _type: SCENE_TYPES.PLAYABLE,
            animation: new MtlAnimation(this.shared_state),
            sound_track: new MtlSoundTrack(this.shared_state),
            choices: [],
            text_box: "",
            game_objects: [],
            cinematic_duration: 0,
            end_cinematic_options: {
                destination_scene_index: -1,
                quit: false,
            }
        };

        this.name = default_data.name;
        this._type = default_data._type;
        this.animation = default_data.animation;
        this.sound_track = default_data.sound_track;

        this.choices = default_data.choices;
        this.text_box = default_data.text_box;
        this.game_objects = default_data.game_objects;

        this.cinematic_duration = default_data.cinematic_duration || 0;
        this.end_cinematic_options = default_data.end_cinematic_options;
    }

    /**
     * Populates the instance from a litteral descriptor.
     * Creates and populates the instances of Animation, Soundtrack and GameObjects
     * @param {Object} data 
     */
    load_data(data) {
        this.name = data.name;
        this._type = data._type;

        const animation = new MtlAnimation(this.shared_state);
        animation.init(data.animation);
        this.animation.clear();
        this.animation = animation;
        this.animation.image.src && this.add_loadable(this.animation);

        const sound_track = new MtlSoundTrack(this.shared_state);
        sound_track.init(data.sound_track);
        this.sound_track.clear();
        this.sound_track = sound_track;
        this.sound_track.src && this.add_loadable(this.sound_track);

        this.choices = data.choices.map(c => new MtlChoice(c));
        this.text_box = data.text_box;
        this.cinematic_duration = data.cinematic_duration || 0;

        this.game_objects = data.game_objects.map(gob => {
            const inst = new MtlGameObject(this.shared_state);
            inst.load_data(gob);
            this.add_loadable(inst);
            return inst;
        });

        this.end_cinematic_options = data.end_cinematic_options;
    }
}

module.exports = MtlScene;