# Mentalo Engine

## The game engine that runs the creations made with Mentalo app.

**Mentalo games are simple "old school" games based on static screens and text choices made by the player.**

## Usage

The Mentalo Engine is integrated as part of the Mentalo app, so it's designed to work with it. However, you can decide to integrate that engine anywhere you like too. In a web page or a Nodejs project in an Electron window for example.

All you have to do is import the module and create an instance of MentaloEngine with a few parameters :

```js
const engine = new MentaloEngine({
    game_data: the_game_data,
    container: document.getElementById("my-game-container-div"),
    fullscreen: true,
    frame_rate: some_fps_value,
    on_quit_game: an_exit_game_callback,
});
```

Then you'll have to init the engine with

```js
engine.init();
```

And run the game you've loaded in paramters with

```js
engine.run_game();
```

### Parameters:

-   **game_data**: `Object` [REQUIRED]

The game data paramter is a single litteral object that holds the game you want to run. You can obtain this object when you load an .mtl file and unserialize it. .mtl file are the files you get when you export your game project with the Mentalo app. The .mtl mime is just a way to make sure the file is a Mentalo file, but the syntax is simple json, so you can unserialize the data like any json data with `JSON.parse(your_mtl_file_loaded_data)`.

-   **container**: `HtmlNode` [OPTIONAL]

The container param must hold a reference to the DOM element you choose to contain the game. So any empty div will do the job. The game engine will create its canvas inside of it. If the container is not provided, a default container will be created and appended to the DOM body in full window size,  absolute position and z-index=1000.

-   **fullscreen**: `Boolean` [OPTIONAL] `default = false`

This allows you to toogle the window in fullscreen automatically when you run the game.

-   **frame_rate**: `Integer` [OPTIONAL] `default = 30`

Frame rate must be an integer value and it will represent the refreshment frequency of the animation (is the game contains animations though) in fps (frame per second).

-   **on_quit_game**: `Function` [OPTIONAL]

You can define a callback that will execute when the game ends and the engine exits.

### Exit the engine

The engine stop's itself when the game is finished, or when the escape key is pressed.

### Module exports types:

The Mentalo Engine module exports the `MentaloEngine` class, but also some useful internal types: (Those types are widely used in inherited classes in the Mentalo app).

```js
// mentalo-engine index.js
module.exports = {
    MentaloEngine,
    MtlAnimation,
    MtlScene,
    MtlGame,
    MtlSoundTrack,
    SCENE_TYPES,
    MtlChoice,
    MtlGameObject,
    FrameRateController,
    font_tools,
    color_tools,
    shape_tools,
};
```

-   **MentaloEngine**: The MentaloEngine class we already saw up there.
-   **MtlAnimation**: A model of image animation that provides methods to update the animation frame, get the frame dimensions, etc.
-   **MtlScene**: This is the class that represent the data structure for a game scene. It contains the image data, the choices of the scene, the soundtrack, the game objects, etc.
-   **MtlGame**: This is the main data structure for a game representation. It holds the state of the game, an array of all the scenes and metadata about the interface design, color, fonts, etc.
-   **MtlSoundtrack**: A small data structure to manage an audio resource.
-   **SCENE_TYPES**: An "enum like" object that represents the different possible kind of scene :

```js
{
    PLAYABLE: "Playable",
    CINEMATIC: "Cinematic",
}
```

-   **MtlChoice**: The data structure for a choice (the element that's clicked by the player on each scene). A choice defines the next scene to display, or if the game must finish or if an object must be used in the inventory, etc.

-   **MtlGameObject**: A game object is holded by a scene, it has an image and a position. And it can be referenced by the inventory state of the game when the player "picks" the object.

-   **FrameRateController**: This is a small convenient mecanism to control the rate of a loop.

-   **font_tools**: This is a set of useful functions to work with fonts:

```js
{
    get_canvas_font,
        get_font_options,
        get_canvas_char_size,
        FONT_FAMILIES,
        TEXT_ALIGN_OPTIONS,
        FONT_STYLE_OPTIONS,
        FONT_WEIGHT_OPTIONS;
}
```

---> get_canvas_font: returns a string formatted to set the CanvasRenderingContext2d.font property. It takes a descriptor object as a parameter with the values for text align, font, etc

---> get_font_options: returns an objects with all the constant array FONT_FAMILIES, TEXT_ALIGN, etc...

---> get_canvas_char_size: Returns an object {width, height} with approximative dimensions of a single character for the CanvasRenderingContext2d object given as param.

---> FONT_FAMILIES, TEXT_ALIGN_OPTIONS, FONT_STYLE_OPTIONS and FONT_WEIGHT_OPTIONS are simple arrays of constants used by the app to format textes.

-   **color_tools**: Exports a set of useful functions to work with colors:

```js
 {
    get_average_rgb_color_tone,
    get_optimal_visible_foreground_color,
    color_str_to_rgb_array,
    rgb_array_to_hex,
    rgba_array_to_hex,
    same_rgba,
}
```

---> get_average_rgb_color_tone: returns an average number of the 3 r g b values.

---> get_optimal_visible_foreground_color: Gets a color in hexadecimal representation as parameter an returns another hex color calculated to be optimally visible on top of the paramter color. The output will be either a lightened or a darkened version of the input color.

---> color_str_to_rgb_array: Takes a hexadecimal representation of a color as an input and returns an array of 3 integer values for r g b.

---> rgb_array_to_hex: Takes an array of 3 integers (from 0 to 255) as an input and returns the equivalent hexadecimal string.

---> rgba_array_to_hex: Same as rgb_array_to_hex but with a fourth number (from 0 to 1) for the alpha channel.

---> same_rgba: Returns a simple comparison result between 2 arrays like [r, g, b, a].

-   **shape_tools**: This just exposes a `draw_rect` function that handles roudned corners and borders:

```js
function draw_rect(ctx, x, y, width, height, options = {
    rounded_corners_radius: 0,
    border: { width: 0, color: "rgba(0,0,0,0)" },
    fill_color: "black",
})
```
