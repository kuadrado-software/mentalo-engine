"use strict";

const { get_canvas_font, get_canvas_char_size } = require("./font-tools");

/**
 * A utility function to display text inside of a box coordinates
 * @param {CanvasRenderingContext2D} ctx
 * @param {String[]} lines 
 * @param {Object} bounds A bounding box object like {left, right, top, bottom, width, height}
 * @param {Object} settings An object with styling settings like {padding, font_color, background_color, ...}
 * @param {Object} a_state_ref Any object having a persistent life in the calling instance
 * @param {Boolean} streamed Wether the text should be displayed as a progressive text streaming or all at once
 * @returns {Boolean} true if the text is fully displayed
 */
function draw_text_in_bounds(ctx, lines, bounds, settings, a_state_ref, streamed) {
    ctx.save();
    const char_size = get_canvas_char_size(ctx, settings);
    const line_height = char_size.text_line_height;
    ctx.font = get_canvas_font(settings);
    ctx.fillStyle = settings.font_color;
    ctx.textAlign = settings.text_align;
    ctx.textBaseline = "top";

    const get_text_position = () => {
        const { padding = 0 } = settings;
        const { left, top, width } = bounds;
        switch (ctx.textAlign) {
            case "left":
                return {
                    x: left + padding,
                    y: top + padding,
                }
            case "right":
                return {
                    x: left + width - padding,
                    y: top + padding,
                }
            case "center":
                return {
                    x: left + width / 2,
                    y: top + padding,
                }
            default:
                return {
                    x: left + padding,
                    y: top + padding,
                }
        }
    };

    const text_pos = get_text_position();

    a_state_ref.stream = a_state_ref.stream || {
        line_chars: 0,
        line_index: 0,
        complete: false,
    };

    const output_lines = streamed ? lines.map((line, i) => {
        const stream_state = a_state_ref.stream;
        if (i < stream_state.line_index || stream_state.complete) {
            return line;
        } else if (i > stream_state.line_index) {
            return "";
        } else {
            const slice = line.slice(0, ++stream_state.line_chars);
            if (slice.length === line.length) {
                stream_state.complete = lines.length - 1 === stream_state.line_index;
                stream_state.line_index = stream_state.complete
                    ? stream_state.line_index
                    : stream_state.line_index + 1;
                stream_state.line_chars = 0;
            }
            return slice;
        }
    }) : (() => {
        a_state_ref.stream.complete = true;
        return lines;
    })();

    output_lines.forEach(line => {
        ctx.fillText(line, text_pos.x, text_pos.y);
        text_pos.y += line_height;
    });

    ctx.restore();

    return a_state_ref.stream.complete;
}

module.exports = {
    draw_text_in_bounds,
}