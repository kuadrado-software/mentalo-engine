"use strict";

/**
 * A structure to manage a group of object that extends from the ./Loadable class.
 * For each handled loadable, increments a loadable_elements state, and once the loadable has finished 
 * to load whatever it needs to load, the loaded_elements state is incremented.
 */
class LoadableGroup {
    constructor() {
        this.loadable_elements = 0;
        this.loaded_elements = 0;
    }

    /**
     * Attaches a on_load callback to the Loadable object given as argument.
     * Increments the loadable_elements state.
     * @param {Loadable} object Any object that extends the Loadable class
     */
    add_loadable(object) {
        this.loadable_elements++;
        /**
         * The callback that will be call by the Loadable object when it will have finished to load what it needs to load.
         */
        object.on_load(() => {
            this.loaded_elements++;
            if (this.loadable_elements === this.loaded_elements && this.on_load_group_complete_custom_callback) {
                this.on_load_group_complete_custom_callback();
            }
        });
    }

    /**
     * Defines a custom callback to call when all handled loadable objects have finished loading.
     * @param {Function} callback 
     */
    on_load_group_complete(callback) {
        this.on_load_group_complete_custom_callback = callback;
    }
}

module.exports = LoadableGroup;