"use strict";

const { draw_text_in_bounds } = require("../../lib/text-tools");
const { get_canvas_char_size } = require("../../lib/font-tools");
const { draw_rect } = require("../../lib/shape-tools");
const MtlUiComponent = require("./ui-component");

/**
 * The component that handles the displaying of a scene text boxe.
 */
class TextBoxCpt extends MtlUiComponent {
    constructor(params) {
        super(params);
        this.set_str_id("TextBoxCpt");
        this.state.text = {};
    }

    /**
     * Initialize precalculations for the text box, text dimensions, split lines, etc.
     */
    init() {
        const { text, settings, bounding_zone } = this.params;

        const char_size = get_canvas_char_size(window.mentalo_drawing_context, settings);

        const line_height = char_size.text_line_height;
        const error_offset = 3 * char_size.width;
        const chars_per_line = (bounding_zone.width - error_offset) / char_size.width;
        const output_lines = [];
        const lines = text.split("\n");

        lines.forEach(line => {
            const res = [""];
            let target_i = 0;
            for (const word of line.split(" ")) {
                if ((res[target_i] + word + " ").length > chars_per_line) {
                    res.push("");
                    target_i++;
                }
                res[target_i] += word + " ";
            }

            res.forEach(l => output_lines.push(l));
        });

        const text_height = output_lines.length * line_height - char_size.interline_height;

        const updated_bounds = Object.assign({ ...bounding_zone }, {
            height: bounding_zone.height + text_height,
            top: bounding_zone.top - text_height,
        });

        this.state.text = {
            lines: output_lines,
            line_height,
            bounding_zone: updated_bounds,
        };

        // Update closing icon position
        const closing_icon = this.children[0];
        let closing_icon_bounds = closing_icon.params.bounding_zone;
        closing_icon_bounds = Object.assign(closing_icon_bounds, {
            top: closing_icon_bounds.top - text_height,
            bottom: closing_icon_bounds.bottom - text_height,
        });

        closing_icon.params.center.y -= text_height;

        this.state.initialized = true;
    }

    /**
     * Draw the text box and the text inside of it.
     * The text is not drawn at once, the characters are drawn one by one at each frame 
     * and a local text.stream state is updated until text is complete.
     */
    draw() {
        if (!this.params.get_visibility_state()) {
            return;
        }

        super.draw();

        const { settings } = this.params;

        if (!this.state.initialized) {
            this.init();
        }

        const ctx = window.mentalo_drawing_context;
        const { lines, bounding_zone } = this.state.text;

        const box_pos = { x: bounding_zone.left, y: bounding_zone.top };
        const box_width = bounding_zone.width;
        const box_height = bounding_zone.height;

        draw_rect(ctx, box_pos.x, box_pos.y, box_width, box_height, {
            fill_color: settings.background_color,
            rounded_corners_radius: settings.rounded_corners_radius,
            border: {
                width: settings.border_width,
                color: settings.font_color,
            },
        });

        const complete = draw_text_in_bounds(ctx, lines, bounding_zone, settings, this.state.text, true);

        if (complete) { // draw closing_icon if text is entirely displayed
            this.draw_children();
        }
    }
}

module.exports = TextBoxCpt;